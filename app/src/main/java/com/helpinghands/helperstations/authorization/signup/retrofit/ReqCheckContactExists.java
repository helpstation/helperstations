package com.helpinghands.helperstations.authorization.signup.retrofit;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.helpinghands.helperstations.hmutils.base.BaseRetroParameters;

public class ReqCheckContactExists extends BaseRetroParameters {

    @SerializedName("otp")
    @Expose
    private String otpNumber = "";

    public ReqCheckContactExists(Context context) {
        super(context);
    }

    public String getOtpNumber() {
        return otpNumber;
    }

    public void setOtpNumber(String otpNumber) {
        this.otpNumber = otpNumber;
    }

}
