package com.helpinghands.helperstations.authorization.login.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReqUserDetails {

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("access_token")
    @Expose
    private String access_token;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
