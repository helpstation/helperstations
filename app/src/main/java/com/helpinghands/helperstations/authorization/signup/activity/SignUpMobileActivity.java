package com.helpinghands.helperstations.authorization.signup.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.CredentialsClient;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.authorization.signup.retrofit.ReqCheckContactExists;
import com.helpinghands.helperstations.authorization.signup.retrofit.ReqUserCreate;
import com.helpinghands.helperstations.authorization.signup.retrofit.ResCheckContactExists;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.util.Random;

import retrofit2.Response;

import static com.helpinghands.helperstations.hmutils.base.BaseFragment.isNeedRetroCallback;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_checkContactExists;

public class SignUpMobileActivity extends BaseActivity {

    private Button btnGetOTP;
    private ReqUserCreate reqUserCreate = new ReqUserCreate();
    private String otpNumber = "",countryCode = "";
    private CountryCodePicker countryCodePicker;
    private AppCompatEditText appCompatEditMobileNumber;

    @Override
    public int setLayout() {
        return R.layout.activity_signup_enter_mobile;
    }

    @Override
    public Activity getCurrentActivity() {
        return SignUpMobileActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return SignUpMobileActivity.this;
    }

    @Override
    public void initView() {

        if (getIntent() != null && getIntent().hasExtra(ReqUserCreate.class.getSimpleName())) {
            reqUserCreate = getIntent().getParcelableExtra(ReqUserCreate.class.getSimpleName());
        }
        btnGetOTP = findViewById(R.id.btnGetOTP);
        btnGetOTP.setOnClickListener(this);

        countryCodePicker = findViewById(R.id.ccp);
        appCompatEditMobileNumber = findViewById(R.id.appCompatEditMobileNumber);
        countryCodePicker.registerPhoneNumberTextView(appCompatEditMobileNumber);
        countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                countryCode = selectedCountry.getPhoneCode();
            }
        });

        countryCode = countryCodePicker.getDefaultCountryCode();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.btnGetOTP:
                if (TextUtils.isEmpty(appCompatEditMobileNumber.getText().toString().trim())) {
                    showSnakbar("Please enter mobile number", false, getParentView());
                    return;
                }

                otpNumber = String.valueOf(createRandomInteger(100000, 999999L));
                ReqCheckContactExists reqCheckContactExists = new ReqCheckContactExists(getCurrentActivity());
                reqCheckContactExists.setUsername(countryCode.concat(appCompatEditMobileNumber.getText().toString().trim()));
                reqCheckContactExists.setOtpNumber(otpNumber);
                isNeedRetroCallback = true;
                checkContactExists(reqCheckContactExists);
                break;
        }
    }

    @Override
    public void startCode() {


    }

    @Override
    public void onRetroSuccess(String method, Object object) {
        super.onRetroSuccess(method, object);

        switch (method) {
            case api_checkContactExists:
                stopProgressDialog();
                Response<ResCheckContactExists> resCheckContactExistsResponse = (Response<ResCheckContactExists>) object;
                if (resCheckContactExistsResponse != null && resCheckContactExistsResponse.body().getStatus().equalsIgnoreCase(API_STATUS_SUCCESS)) {

                    if (getIntent().getStringExtra("Context").equals("Forgot")) {
                        showSnakbar("User does not exist. Please register with HelperStations.", true, getParentView());
                        return;
                    }

                    String otp_message = getResources().getString(R.string.tv_label_verification_code).concat(appCompatEditMobileNumber.getText().toString().trim());
                    if (!TextUtils.isEmpty(resCheckContactExistsResponse.body().getResponse())) {
                        otp_message = resCheckContactExistsResponse.body().getResponse();
                    }

                    reqUserCreate.setMobile(countryCode.concat(appCompatEditMobileNumber.getText().toString().trim()));

                    if (getIntent().hasExtra("FirebaseToken")) {
                        reqUserCreate.setFirebaseToken(getIntent().getStringExtra("FirebaseToken"));
                    }

                    startActivityForResult(new Intent(getCurrentActivity(), SignUpVerificationActivity.class)
                            .putExtra(ReqUserCreate.class.getSimpleName(), reqUserCreate)
                            .putExtra("OtpNumber", otpNumber)
                            .putExtra("Context", getIntent().getStringExtra("Context"))
                            .putExtra("OTP_MESSAGE", otp_message), INTENT_RQ_RESPONSE_OTP);

                } else if (resCheckContactExistsResponse != null && resCheckContactExistsResponse.body().getStatus().equalsIgnoreCase(API_STATUS_FAILED)) {

                    if (getIntent().getStringExtra("Context").equals("Forgot")) {
                        reqUserCreate.setMobile(countryCode.concat(appCompatEditMobileNumber.getText().toString().trim()));
                        startActivityForResult(new Intent(getCurrentActivity(), SignUpVerificationActivity.class)
                                .putExtra(ReqUserCreate.class.getSimpleName(), reqUserCreate)
                                .putExtra("OtpNumber", otpNumber)
                                .putExtra("Context", getIntent().getStringExtra("Context"))
                                .putExtra("OTP_MESSAGE", "Please enter the six digit verification code below which we have sent to ".concat(appCompatEditMobileNumber.getText().toString().trim())), INTENT_RQ_RESPONSE_OTP);
                    } else {
                        showSnakbar(resCheckContactExistsResponse.body().getResponse(), true, getParentView());
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case INTENT_RQ_RESPONSE_OTP:
                if (resultCode == RESULT_OK) {
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_mobile_number, appCompatEditMobileNumber.getText().toString().trim());
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }

    @Override
    public void onRetroFailure(String method, Object object) {
        super.onRetroFailure(method, object);
        startActivityForResult(new Intent(getCurrentActivity(), SignUpVerificationActivity.class)
                .putExtra(ReqUserCreate.class.getSimpleName(), reqUserCreate)
                .putExtra("OtpNumber", otpNumber)
                .putExtra("Context", getIntent().getStringExtra("Context"))
                .putExtra("OTP_MESSAGE", "messageSend"), INTENT_RQ_RESPONSE_OTP);

    }

    @Override
    public String setToolbarTitle() {
        return "";
    }

    /* *****************************************   Random number generator function for generate OTP ****************************************** */
    public long createRandomInteger(int aStart, long aEnd) {
        Random aRandom = new Random();
        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        long range = aEnd - (long) aStart + 1;
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * aRandom.nextDouble());
        long randomNumber = fraction + (long) aStart;
        return randomNumber;
    }
}
