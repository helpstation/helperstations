package com.helpinghands.helperstations.authorization.signup.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.authorization.login.retrofit.ReqOauthTokenLogin;
import com.helpinghands.helperstations.authorization.login.retrofit.ResOauthTokenLogin;
import com.helpinghands.helperstations.authorization.signup.retrofit.ReqUserCreate;
import com.helpinghands.helperstations.authorization.signup.retrofit.ResUserCreate;
import com.helpinghands.helperstations.dashboard.activitiy.DashboardActivity;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.hmutils.preferences.LocationData;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;

import java.util.UUID;

import retrofit2.Response;

import static com.helpinghands.helperstations.hmutils.base.BaseFragment.isNeedRetroCallback;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_oauthToken;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_userCreate;

public class SignUpOtherDetailsActivity extends BaseActivity {

    private ReqUserCreate reqUserCreate;
    private TextInputLayout tilFirstname;
    private TextInputLayout tilLastname;
    private RadioButton rbMale;
    private RadioButton rbFemale;
    private RadioButton rbOthers;
    private TextInputLayout tilPassword;
    private TextInputLayout tilEmailAddress;
    private TextInputEditText edtBirthDate;
    private Button btnSignUp;
    private String gender = "MALE";

    @Override
    public int setLayout() {
        return R.layout.activity_signup_other_details;
    }

    @Override
    public Activity getCurrentActivity() {
        return SignUpOtherDetailsActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return SignUpOtherDetailsActivity.this;
    }

    @Override
    public void initView() {
        if (getIntent() != null) {
            reqUserCreate = getIntent().getParcelableExtra(ReqUserCreate.class.getSimpleName());
        }

        tilFirstname = findViewById(R.id.tilFirstname);
        tilLastname = findViewById(R.id.tilLastname);
        tilPassword = findViewById(R.id.tilPassword);
        tilEmailAddress = findViewById(R.id.tilEmailAddress);
        rbMale = findViewById(R.id.rbMale);
        rbMale.setOnClickListener(this);
        rbFemale = findViewById(R.id.rbFemale);
        rbFemale.setOnClickListener(this);
        rbOthers = findViewById(R.id.rbOthers);
        rbOthers.setOnClickListener(this);
        edtBirthDate = findViewById(R.id.edtBirthDate);
        edtBirthDate.setOnClickListener(this);
        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnSignUp:
                validateAndRegisterUser();
                break;

            case R.id.edtBirthDate:
                showDatePickerDialog(edtBirthDate, dd_MMM_yyyy_NO_DASH);
                break;

            case R.id.rbMale:
                rbMale.setChecked(true);
                rbFemale.setChecked(false);
                rbOthers.setChecked(false);
                gender = "MALE";
                break;

            case R.id.rbFemale:
                rbMale.setChecked(false);
                rbFemale.setChecked(true);
                rbOthers.setChecked(false);
                gender = "FEMALE";
                break;

            case R.id.rbOthers:
                rbMale.setChecked(false);
                rbFemale.setChecked(false);
                rbOthers.setChecked(true);
                gender = "OTHERS";
                break;
        }
    }

    private void validateAndRegisterUser() {

        if (TextUtils.isEmpty(tilFirstname.getEditText().getText().toString().trim())) {
            showSnakbar("Please enter your First Name", false, getParentView());
            return;
        }

        if (TextUtils.isEmpty(tilLastname.getEditText().getText().toString().trim())) {
            showSnakbar("Please enter your Last Name", false, getParentView());
            return;
        }

        if (TextUtils.isEmpty(tilPassword.getEditText().getText().toString().trim())) {
            showSnakbar("Please enter your Password", false, getParentView());
            return;
        }

        if (TextUtils.isEmpty(tilEmailAddress.getEditText().getText().toString().trim())) {
            showSnakbar("Please enter your Email Address", false, getParentView());
            return;
        }

        if (TextUtils.isEmpty(edtBirthDate.getText().toString().trim())) {
            showSnakbar("Please enter your BirthDate", false, getParentView());
            return;
        }

        reqUserCreate.setCurrentLocation(LocationData.getInstance().currentLocationAddress);
        reqUserCreate.setDateOfBirth(dateFormat(edtBirthDate.getText().toString().trim(), dd_MMM_yyyy_NO_DASH, yyyy_MM_dd, false));
        reqUserCreate.setEmail(tilEmailAddress.getEditText().getText().toString().trim());
        reqUserCreate.setFirstName(tilFirstname.getEditText().getText().toString().trim());
        reqUserCreate.setGender(gender);
        reqUserCreate.setLastName(tilLastname.getEditText().getText().toString().trim());
        reqUserCreate.setLatitudes(LocationData.getInstance().currentlatitude);
        reqUserCreate.setLongitudes(LocationData.getInstance().currentlongitude);
        reqUserCreate.setPassword(tilPassword.getEditText().getText().toString().trim());
        reqUserCreate.setUsername(reqUserCreate.getMobile());
        reqUserCreate.setDeviceId(UUID.randomUUID().toString());

        isNeedRetroCallback = true;
        userCreate(reqUserCreate);
    }

    @Override
    public void startCode() {


    }

    @Override
    public void onRetroSuccess(String method, Object object) {
        super.onRetroSuccess(method, object);

        switch (method) {
            case api_userCreate:
                Response<ResUserCreate> createResponse = (Response<ResUserCreate>) object;

                if (createResponse != null && createResponse.body().getStatus().equalsIgnoreCase(API_STATUS_SUCCESS)) {

                    Preferences.getPreference().addBoolean(getCurrentContext(), R.string.pref_userLoggedIn, true);
                    Preferences.getPreference().addBoolean(getCurrentContext(), R.string.pref_userLoggedIn_via_google, false);
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_username,
                            tilFirstname.getEditText().getText().toString().trim().concat(" ").concat(
                                    tilLastname.getEditText().getText().toString().trim()));
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_first_name, tilFirstname.getEditText().getText().toString().trim());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_last_name, tilLastname.getEditText().getText().toString().trim());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_email_address, tilEmailAddress.getEditText().getText().toString().trim());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_mobile_number, reqUserCreate.getMobile());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_birth_date, edtBirthDate.getText().toString().trim());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_primary_registered_user_id, createResponse.body().getPrimary_user_id());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_gender, gender);

                    ReqOauthTokenLogin reqOauthTokenLogin = new ReqOauthTokenLogin();
                    reqOauthTokenLogin.setUsername(reqUserCreate.getMobile());
                    reqOauthTokenLogin.setPassword(tilPassword.getEditText().getText().toString().trim());
                    oauthTokenLogin(reqOauthTokenLogin);
                } else {
                    stopProgressDialog();
                    showSnakbar(createResponse.body().getResponse(), true, getParentView());
                }
                break;

            case api_oauthToken:
                stopProgressDialog();
                Response<ResOauthTokenLogin> resOauthTokenLoginResponse = (Response<ResOauthTokenLogin>) object;

                if (resOauthTokenLoginResponse != null && !TextUtils.isEmpty(resOauthTokenLoginResponse.body().getAccess_token())) {
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_auth_token, resOauthTokenLoginResponse.body().getAccess_token());
                    startActivity(new Intent(getCurrentActivity(), DashboardActivity.class).putExtra("NewUser", true)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    );
                } else {
                    showSnakbar(resOauthTokenLoginResponse.body().getError_description(), true, getParentView());
                }
                break;
        }
    }

    @Override
    public void onRetroFailure(String method, Object object) {
        super.onRetroFailure(method, object);
        stopProgressDialog();
    }

    @Override
    public String setToolbarTitle() {
        return "";
    }
}
