package com.helpinghands.helperstations.authorization.resetuserauth.retrofit;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.helpinghands.helperstations.hmutils.base.BaseRetroParameters;

public class ReqForgotUpdatePassword extends BaseRetroParameters {

    @SerializedName("password")
    @Expose
    private String password = "";

    public ReqForgotUpdatePassword(Context context) {
        super(context);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
