package com.helpinghands.helperstations.authorization.login.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.authorization.login.retrofit.ReqGetGoogleAuth;
import com.helpinghands.helperstations.authorization.login.retrofit.ReqOauthTokenLogin;
import com.helpinghands.helperstations.authorization.login.retrofit.ReqUserDetails;
import com.helpinghands.helperstations.authorization.login.retrofit.ResGetGoogleAuth;
import com.helpinghands.helperstations.authorization.login.retrofit.ResOauthTokenLogin;
import com.helpinghands.helperstations.authorization.login.retrofit.ResUserDetails;
import com.helpinghands.helperstations.authorization.signup.activity.SignUpMobileActivity;
import com.helpinghands.helperstations.authorization.signup.retrofit.ReqUserCreate;
import com.helpinghands.helperstations.dashboard.activitiy.DashboardActivity;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.hmutils.preferences.LocationData;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;

import java.util.UUID;

import retrofit2.Response;

import static com.helpinghands.helperstations.hmutils.base.BaseFragment.isNeedRetroCallback;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_oauthToken;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_profileUserDetail;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_userGoogleSignUp;

public class LoginActivity extends BaseActivity {

    private TextInputLayout tilUsername;
    private TextInputLayout tilPassword;
    private CheckBox cbRememberMe;
    private Button btnLogin;
    private TextView tvOr;
    private ImageView ivFacebook;
    private ImageView ivInstagram;
    private ImageView ivGoogle;
    private RelativeLayout relativeRegister;
    private TextView tvNoMember;
    private TextView tvRegister;
    private TextView tvForgotPassword;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInAccount googleAccount;
    private String googleUserName = "";

    @Override
    public int setLayout() {
        return R.layout.activity_login;
    }


    @Override
    public Activity getCurrentActivity() {
        return LoginActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return LoginActivity.this;
    }

    @Override
    public void initView() {

        removeGoogleAuth();
        tilUsername = findViewById(R.id.tilUsername);
        tilPassword = findViewById(R.id.tilPassword);
        cbRememberMe = findViewById(R.id.cbRememberMe);
        btnLogin = findViewById(R.id.btnLogin);
        tvOr = findViewById(R.id.tvOr);
        ivFacebook = findViewById(R.id.ivFacebook);
        ivInstagram = findViewById(R.id.ivInstagram);
        ivGoogle = findViewById(R.id.ivGoogle);
        ivGoogle.setOnClickListener(this);
        relativeRegister = findViewById(R.id.relativeRegister);
        tvNoMember = findViewById(R.id.tvNoMember);
        tvRegister = findViewById(R.id.tvRegister);
        tvRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setOnClickListener(this);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken splash", newToken);
                newFirebaseToken = newToken;
//                tilUsername.getEditText().setText(newToken);
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tvRegister:
                startActivity(new Intent(getCurrentContext(), SignUpMobileActivity.class)
                        .putExtra("FirebaseToken", newFirebaseToken)
                        .putExtra("Context", "Register"));
                break;

            case R.id.btnLogin:

                if (TextUtils.isEmpty(tilUsername.getEditText().getText().toString().trim())) {
                    showSnakbar("Please enter your mobile number", false, getParentView());
                    return;
                }

                if (TextUtils.isEmpty(tilPassword.getEditText().getText().toString().trim())) {
                    showSnakbar("Please enter your password", false, getParentView());
                    return;
                }

                googleUserName = tilUsername.getEditText().getText().toString().trim();
                ReqOauthTokenLogin reqOauthTokenLogin = new ReqOauthTokenLogin();
                reqOauthTokenLogin.setUsername(tilUsername.getEditText().getText().toString().trim());
                reqOauthTokenLogin.setPassword(tilPassword.getEditText().getText().toString().trim());
                isNeedRetroCallback = true;
                oauthTokenLogin(reqOauthTokenLogin);
                break;

            case R.id.tvForgotPassword:
                startActivity(new Intent(getCurrentContext(), SignUpMobileActivity.class)
                        .putExtra("Context", "Forgot"));
                break;

            case R.id.ivGoogle:
                initGoogleSignIn();
                break;
        }
    }

    private void initGoogleSignIn() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        googleAccount = GoogleSignIn.getLastSignedInAccount(this);
        if (googleAccount == null) {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, INTENT_RC_SIGN_IN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case INTENT_RC_SIGN_IN:
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleGoogleSignInResult(task);
                break;
        }
    }

    private void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            googleAccount = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            if (googleAccount != null) {

                ReqGetGoogleAuth reqGetGoogleAuth = new ReqGetGoogleAuth();
                reqGetGoogleAuth.setEmailAddress(googleAccount.getEmail());
                isNeedRetroCallback = true;
                getGoogleAuth(reqGetGoogleAuth);
            }
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    @Override
    public void startCode() {

    }

    @Override
    public void onRetroSuccess(String method, Object object) {
        super.onRetroSuccess(method, object);
        switch (method) {

            case api_userGoogleSignUp:

                Response<ResGetGoogleAuth> createResponse = (Response<ResGetGoogleAuth>) object;

                if (createResponse != null && !TextUtils.isEmpty(createResponse.body().getResponse().getUsername())) {
                    Preferences.getPreference().addBoolean(getCurrentContext(), R.string.pref_userLoggedIn_via_google, true);
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_email_address, googleAccount.getEmail());
                    ReqOauthTokenLogin reqOauthTokenLogin = new ReqOauthTokenLogin();
                    reqOauthTokenLogin.setUsername(createResponse.body().getResponse().getUsername());
                    reqOauthTokenLogin.setPassword(createResponse.body().getResponse().getUsername());
                    googleUserName = createResponse.body().getResponse().getUsername();
                    isNeedRetroCallback = true;
                    oauthTokenLogin(reqOauthTokenLogin);

                } else {

                    ReqUserCreate reqUserCreate = new ReqUserCreate();
                    reqUserCreate.setCurrentLocation(LocationData.getInstance().currentLocationAddress);
                    reqUserCreate.setDateOfBirth("");
                    reqUserCreate.setEmail(googleAccount.getEmail());
                    reqUserCreate.setFirstName(googleAccount.getGivenName());
                    reqUserCreate.setGender("MALE");
                    reqUserCreate.setLastName("");
                    reqUserCreate.setLatitudes(LocationData.getInstance().currentlatitude);
                    reqUserCreate.setLongitudes(LocationData.getInstance().currentlongitude);
                    reqUserCreate.setPassword(tilPassword.getEditText().getText().toString().trim());
                    reqUserCreate.setUsername(googleAccount.getDisplayName());
                    reqUserCreate.setDeviceId(UUID.randomUUID().toString());
                    reqUserCreate.setFirebaseToken(newFirebaseToken);
                    reqUserCreate.setProfilPicPath(googleAccount.getPhotoUrl() != null ? googleAccount.getPhotoUrl().toString() : "");

                    stopProgressDialog();
                    startActivity(new Intent(getCurrentActivity(), SignUpMobileActivity.class)
                            .putExtra("Context", "isGoogleUser")
                            .putExtra(ReqUserCreate.class.getSimpleName(), reqUserCreate));
                }
                break;

            case api_oauthToken:

                Response<ResOauthTokenLogin> resOauthTokenLoginResponse = (Response<ResOauthTokenLogin>) object;

                if (resOauthTokenLoginResponse != null && !TextUtils.isEmpty(resOauthTokenLoginResponse.body().getAccess_token())) {
                    Preferences.getPreference().addBoolean(getCurrentContext(), R.string.pref_userLoggedIn_via_google, false);
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_auth_token, resOauthTokenLoginResponse.body().getAccess_token());

                    ReqUserDetails reqUserDetails = new ReqUserDetails();
                    reqUserDetails.setUsername(googleUserName);
                    reqUserDetails.setAccess_token(resOauthTokenLoginResponse.body().getAccess_token());
                    isNeedRetroCallback = true;
                    getUserDetails(reqUserDetails);

                } else {
                    removeGoogleAuth();
                    stopProgressDialog();
                    showSnakbar(resOauthTokenLoginResponse.body().getError_description(), true, getParentView());
                }
                break;

            case api_profileUserDetail:
                stopProgressDialog();
                Response<ResUserDetails> resUserDetailsResponse = (Response<ResUserDetails>) object;

                Preferences.getPreference().addBoolean(getCurrentContext(), R.string.pref_userLoggedIn, true);
                Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_username, resUserDetailsResponse.body().getResponse().getFirstName().concat(" ").concat(resUserDetailsResponse.body().getResponse().getLastName()));
                Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_first_name, resUserDetailsResponse.body().getResponse().getFirstName());
                Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_last_name, resUserDetailsResponse.body().getResponse().getLastName());
                Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_mobile_number, resUserDetailsResponse.body().getResponse().getMobile());
                Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_birth_date, resUserDetailsResponse.body().getResponse().getDateOfBirth());
                Preferences.getPreference().addString(getCurrentContext(), R.string.pref_primary_registered_user_id, "");
                Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_gender, resUserDetailsResponse.body().getResponse().getGender());
                Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_profile_pic_path, resUserDetailsResponse.body().getResponse().getProfilPicPath());

                startActivity(new Intent(getCurrentActivity(), DashboardActivity.class).putExtra("NewUser", true));
                finish();
                break;
        }
    }

    @Override
    public void onRetroFailure(String method, Object object) {
        super.onRetroFailure(method, object);
        removeGoogleAuth();
    }

    @Override
    public String setToolbarTitle() {
        return "";
    }
}
