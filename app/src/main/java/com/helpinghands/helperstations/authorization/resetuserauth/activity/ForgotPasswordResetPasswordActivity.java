package com.helpinghands.helperstations.authorization.resetuserauth.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputLayout;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.authorization.resetuserauth.retrofit.ReqForgotUpdatePassword;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.authorization.login.activity.LoginActivity;
import com.helpinghands.helperstations.authorization.resetuserauth.retrofit.ResForgotUpdatePassword;

import retrofit2.Response;

import static com.helpinghands.helperstations.hmutils.base.BaseFragment.isNeedRetroCallback;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_userUpdatePassword;

public class ForgotPasswordResetPasswordActivity extends BaseActivity {

    private Button btnSubmit;
    private TextInputLayout tilPassword;
    private TextInputLayout tilConfirmPassword;

    @Override
    public int setLayout() {
        return R.layout.activity_reset_password;
    }

    @Override
    public Activity getCurrentActivity() {
        return ForgotPasswordResetPasswordActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return ForgotPasswordResetPasswordActivity.this;
    }

    @Override
    public void initView() {

        tilPassword = findViewById(R.id.tilPassword);
        tilConfirmPassword = findViewById(R.id.tilConfirmPassword);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnSubmit:
                if (TextUtils.isEmpty(tilPassword.getEditText().getText().toString().trim())) {
                    showSnakbar("Please enter your new password", false, getParentView());
                    return;
                }
                if (TextUtils.isEmpty(tilConfirmPassword.getEditText().getText().toString().trim())) {
                    showSnakbar("Please confirm your password", false, getParentView());
                    return;
                }
                if (!TextUtils.equals(tilPassword.getEditText().getText().toString().trim(), tilConfirmPassword.getEditText().getText().toString().trim())) {
                    showSnakbar("Confirm password does not match with new password.", false, getParentView());
                    return;
                }
                ReqForgotUpdatePassword reqUserCreate = new ReqForgotUpdatePassword(getCurrentActivity());
                reqUserCreate.setUsername(getIntent().getStringExtra("Mobile"));
                reqUserCreate.setPassword(tilPassword.getEditText().getText().toString().trim());
                isNeedRetroCallback = true;
                forgotUpdatePassword(reqUserCreate);
                break;
        }
    }

    @Override
    public void startCode() {


    }

    @Override
    public void onRetroSuccess(String method, Object object) {

        switch (method) {
            case api_userUpdatePassword:
                stopProgressDialog();
                    Response<ResForgotUpdatePassword> resForgotUpdatePasswordResponse = (Response<ResForgotUpdatePassword>) object;
                if (resForgotUpdatePasswordResponse != null) {

                    startActivity(new Intent(getCurrentContext(), LoginActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                }
                break;
        }
    }

    @Override
    public void onRetroFailure(String method, Object object) {


    }

    @Override
    public String setToolbarTitle() {
        return "";
    }
}
