package com.helpinghands.helperstations.authorization.resetuserauth.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResForgotUpdatePassword {

    @SerializedName("status")
    @Expose
    private String status = "";

    @SerializedName("response")
    @Expose
    private String response = "";

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
