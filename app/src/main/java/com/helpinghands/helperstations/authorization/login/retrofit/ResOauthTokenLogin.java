package com.helpinghands.helperstations.authorization.login.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResOauthTokenLogin {

    @SerializedName("access_token")
    @Expose
    private String access_token = "";

    @SerializedName("token_type")
    @Expose
    private String token_type = "";

    @SerializedName("refresh_token")
    @Expose
    private String refresh_token = "";

    @SerializedName("expires_in")
    @Expose
    private String expires_in = "";

    @SerializedName("scope")
    @Expose
    private String scope = "";

    @SerializedName("jti")
    @Expose
    private String jti = "";

    @SerializedName("error")
    @Expose
    private String error = "";

    @SerializedName("error_description")
    @Expose
    private String error_description = "";


    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }
}
