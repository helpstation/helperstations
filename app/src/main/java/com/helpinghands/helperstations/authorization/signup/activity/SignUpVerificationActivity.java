package com.helpinghands.helperstations.authorization.signup.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputLayout;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.authorization.login.retrofit.ReqOauthTokenLogin;
import com.helpinghands.helperstations.authorization.login.retrofit.ResOauthTokenLogin;
import com.helpinghands.helperstations.authorization.receiver.ReadSmsBroadcastReceiver;
import com.helpinghands.helperstations.authorization.resetuserauth.activity.ForgotPasswordResetPasswordActivity;
import com.helpinghands.helperstations.authorization.signup.retrofit.ReqUserCreate;
import com.helpinghands.helperstations.authorization.signup.retrofit.ResUserCreate;
import com.helpinghands.helperstations.dashboard.activitiy.DashboardActivity;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;

import static com.helpinghands.helperstations.hmutils.base.BaseFragment.isNeedRetroCallback;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_oauthToken;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_userCreate;

public class SignUpVerificationActivity extends BaseActivity {

    private TextInputLayout tilOTP;
    private Button btnSignUp;
    private TextView tvOtpMessage;
    private ReqUserCreate reqUserCreate;
    private ReadSmsBroadcastReceiver readSmsBroadcastReceiver;

    @Override
    public int setLayout() {
        return R.layout.activity_signup_enter_otp;
    }

    @Override
    public Activity getCurrentActivity() {
        return SignUpVerificationActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return SignUpVerificationActivity.this;
    }

    @Override
    public void initView() {

        tilOTP = findViewById(R.id.tilOTP);
        tvOtpMessage = findViewById(R.id.tvOtpMessage);
        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);
        tilOTP.getEditText().setText(getIntent().getStringExtra("otpNumber"));

        if (getIntent() != null) {
            if (getIntent().hasExtra(ReqUserCreate.class.getSimpleName())) {
                reqUserCreate = getIntent().getParcelableExtra(ReqUserCreate.class.getSimpleName());
            }
            if (getIntent().hasExtra("OTP_MESSAGE")) {
                tvOtpMessage.setText(getIntent().getStringExtra("OTP_MESSAGE"));
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnSignUp:
                if (getIntent().hasExtra("OtpNumber")) {
                    if (!tilOTP.getEditText().getText().toString().trim().equalsIgnoreCase(getIntent().getStringExtra("OtpNumber"))) {
                        showSnakbar("Please enter valid OTP", true, getParentView());
                    }
                }
                if (getIntent().getStringExtra("Context").equals("Forgot")) {
                    startActivity(new Intent(getCurrentContext(), ForgotPasswordResetPasswordActivity.class)
                            .putExtra("Mobile", reqUserCreate.getMobile()));
                } else if (getIntent().getStringExtra("Context").equals("isGoogleUser")) {
                    registerGoogleUser();
                } else {
                    startActivity(new Intent(getCurrentContext(), SignUpOtherDetailsActivity.class).putExtra(ReqUserCreate.class.getSimpleName(), reqUserCreate));
                }
                break;
        }
    }

    private void registerGoogleUser() {
        isNeedRetroCallback = true;
        reqUserCreate.setUsername(reqUserCreate.getMobile());
        reqUserCreate.setPassword(reqUserCreate.getMobile());
        userCreate(reqUserCreate);
    }

    @Override
    public void startCode() {
        startSmsUserConsent();
    }

    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        client.startSmsRetriever().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    private void registerBroadcastReceiver() {
        readSmsBroadcastReceiver = new ReadSmsBroadcastReceiver();
        readSmsBroadcastReceiver.readSmsBroadcastReceiverListener =
                new ReadSmsBroadcastReceiver.ReadSmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                        try {
                            if (intent != null && intent.hasExtra("message")) {
                                String message = intent.getExtras().getString("message", "");
                                getOtpFromMessage(message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure() {
                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(readSmsBroadcastReceiver, intentFilter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == REQUEST_USER_CONSENT) {
                if ((resultCode == RESULT_OK) && (data != null)) {
                    String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    getOtpFromMessage(message);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getOtpFromMessage(String message) {
        try {
            Pattern pattern = Pattern.compile("(|^)\\d{6}");
            Matcher matcher = pattern.matcher(message);
            if (matcher.find()) {
                tilOTP.getEditText().setText(matcher.group());
                Toast.makeText(getCurrentActivity(), "OTP verified successfully.", Toast.LENGTH_LONG).show();
                btnSignUp.performClick();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterReceiver(readSmsBroadcastReceiver);
    }

    @Override
    public void onRetroSuccess(String method, Object object) {
        super.onRetroSuccess(method, object);
        switch (method) {
            case api_userCreate:
                Response<ResUserCreate> createResponse = (Response<ResUserCreate>) object;

                if (createResponse != null && createResponse.body().getStatus().equalsIgnoreCase(API_STATUS_SUCCESS)) {

                    Preferences.getPreference().addBoolean(getCurrentContext(), R.string.pref_userLoggedIn, true);
                    Preferences.getPreference().addBoolean(getCurrentContext(), R.string.pref_userLoggedIn_via_google, true);
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_username, reqUserCreate.getFirstName());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_first_name, reqUserCreate.getFirstName());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_last_name, "");
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_email_address, reqUserCreate.getEmail());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_mobile_number, reqUserCreate.getMobile());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_birth_date, "");
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_primary_registered_user_id, "");
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_gender, reqUserCreate.getGender());
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_profile_pic_path, reqUserCreate.getProfilPicPath());

                    ReqOauthTokenLogin reqOauthTokenLogin = new ReqOauthTokenLogin();
                    reqOauthTokenLogin.setUsername(reqUserCreate.getUsername());
                    reqOauthTokenLogin.setPassword(reqUserCreate.getPassword());
                    oauthTokenLogin(reqOauthTokenLogin);
                } else {
                    stopProgressDialog();
                    showSnakbar(createResponse.body().getResponse(), true, getParentView());
                }
                break;

            case api_oauthToken:
                stopProgressDialog();
                Response<ResOauthTokenLogin> resOauthTokenLoginResponse = (Response<ResOauthTokenLogin>) object;

                if (resOauthTokenLoginResponse != null && !TextUtils.isEmpty(resOauthTokenLoginResponse.body().getAccess_token())) {
                    Preferences.getPreference().addString(getCurrentContext(), R.string.pref_api_param_auth_token, resOauthTokenLoginResponse.body().getAccess_token());
                    startActivity(new Intent(getCurrentActivity(), DashboardActivity.class).putExtra("NewUser", true)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                } else {
                    showSnakbar(resOauthTokenLoginResponse.body().getError_description(), true, getParentView());
                }
                break;
        }

    }

    @Override
    public void onRetroFailure(String method, Object object) {
        super.onRetroFailure(method, object);
    }

    @Override
    public String setToolbarTitle() {
        return "";
    }
}
