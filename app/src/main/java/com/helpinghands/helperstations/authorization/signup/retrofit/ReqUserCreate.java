package com.helpinghands.helperstations.authorization.signup.retrofit;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReqUserCreate implements Parcelable {

    public static final Creator<ReqUserCreate> CREATOR = new Creator<ReqUserCreate>() {
        @Override
        public ReqUserCreate createFromParcel(Parcel in) {
            return new ReqUserCreate(in);
        }

        @Override
        public ReqUserCreate[] newArray(int size) {
            return new ReqUserCreate[size];
        }
    };
    @SerializedName("currentLocation")
    @Expose
    private String currentLocation = "";
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth = "";
    @SerializedName("email")
    @Expose
    private String email = "";
    @SerializedName("firstName")
    @Expose
    private String firstName = "";
    @SerializedName("gender")
    @Expose
    private String gender = "";
    @SerializedName("lastName")
    @Expose
    private String lastName = "";
    @SerializedName("latitudes")
    @Expose
    private double latitudes = 0;
    @SerializedName("longitudes")
    @Expose
    private double longitudes = 0;
    @SerializedName("mobile")
    @Expose
    private String mobile = "";
    @SerializedName("password")
    @Expose
    private String password = "";
    @SerializedName("profilPicPath")
    @Expose
    private String profilPicPath = "";
    @SerializedName("username")
    @Expose
    private String username = "";
    @SerializedName("firebaseToken")
    @Expose
    private String firebaseToken = "";
    @SerializedName("deviceId")
    @Expose
    private String deviceId = "";

    public ReqUserCreate() {
    }

    protected ReqUserCreate(Parcel in) {
        currentLocation = in.readString();
        dateOfBirth = in.readString();
        email = in.readString();
        firstName = in.readString();
        gender = in.readString();
        lastName = in.readString();
        latitudes = in.readDouble();
        longitudes = in.readDouble();
        mobile = in.readString();
        password = in.readString();
        profilPicPath = in.readString();
        username = in.readString();
        firebaseToken = in.readString();
        deviceId = in.readString();
    }

    public static Creator<ReqUserCreate> getCREATOR() {
        return CREATOR;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(currentLocation);
        dest.writeString(dateOfBirth);
        dest.writeString(email);
        dest.writeString(firstName);
        dest.writeString(gender);
        dest.writeString(lastName);
        dest.writeDouble(latitudes);
        dest.writeDouble(longitudes);
        dest.writeString(mobile);
        dest.writeString(password);
        dest.writeString(profilPicPath);
        dest.writeString(username);
        dest.writeString(firebaseToken);
        dest.writeString(deviceId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getLatitudes() {
        return latitudes;
    }

    public void setLatitudes(double latitudes) {
        this.latitudes = latitudes;
    }

    public double getLongitudes() {
        return longitudes;
    }

    public void setLongitudes(double longitudes) {
        this.longitudes = longitudes;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfilPicPath() {
        return profilPicPath;
    }

    public void setProfilPicPath(String profilPicPath) {
        this.profilPicPath = profilPicPath;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
