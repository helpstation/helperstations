package com.helpinghands.helperstations.authorization.signup.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResUserCreate {

    @SerializedName("userId")
    @Expose
    private String primary_user_id = "";
    @SerializedName("status")
    @Expose
    private String status = "";
    @SerializedName("response")
    @Expose
    private String response = "";

    public String getPrimary_user_id() {
        return primary_user_id;
    }

    public void setPrimary_user_id(String primary_user_id) {
        this.primary_user_id = primary_user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
