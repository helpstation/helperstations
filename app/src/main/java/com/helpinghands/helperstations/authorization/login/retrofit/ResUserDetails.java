package com.helpinghands.helperstations.authorization.login.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResUserDetails {


    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("response")
    @Expose
    private Response response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public class Response {

        @SerializedName("firstName")
        @Expose
        private String firstName = "";
        @SerializedName("lastName")
        @Expose
        private String lastName = "";
        @SerializedName("mobile")
        @Expose
        private String mobile = "";
        @SerializedName("email")
        @Expose
        private String email = "";
        @SerializedName("currentLocation")
        @Expose
        private String currentLocation = "";
        @SerializedName("latitudes")
        @Expose
        private String latitudes = "";
        @SerializedName("longitudes")
        @Expose
        private String longitudes = "";
        @SerializedName("dateOfBirth")
        @Expose
        private String dateOfBirth = "";
        @SerializedName("gender")
        @Expose
        private String gender = "";
        @SerializedName("profilPicPath")
        @Expose
        private String profilPicPath = "";

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCurrentLocation() {
            return currentLocation;
        }

        public void setCurrentLocation(String currentLocation) {
            this.currentLocation = currentLocation;
        }

        public String getLatitudes() {
            return latitudes;
        }

        public void setLatitudes(String latitudes) {
            this.latitudes = latitudes;
        }

        public String getLongitudes() {
            return longitudes;
        }

        public void setLongitudes(String longitudes) {
            this.longitudes = longitudes;
        }

        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getProfilPicPath() {
            return profilPicPath;
        }

        public void setProfilPicPath(String profilPicPath) {
            this.profilPicPath = profilPicPath;
        }
    }
}
