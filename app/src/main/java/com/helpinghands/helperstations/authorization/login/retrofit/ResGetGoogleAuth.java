package com.helpinghands.helperstations.authorization.login.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResGetGoogleAuth {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("response")
    @Expose
    private Response response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public class Response {

        @SerializedName("username")
        @Expose
        private String username;

        @SerializedName("alreadyExist")
        @Expose
        private boolean alreadyExist;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public boolean getAlreadyExist() {
            return alreadyExist;
        }

        public void setAlreadyExist(boolean alreadyExist) {
            this.alreadyExist = alreadyExist;
        }
    }
}
