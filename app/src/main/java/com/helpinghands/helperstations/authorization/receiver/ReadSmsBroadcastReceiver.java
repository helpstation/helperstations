package com.helpinghands.helperstations.authorization.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

public class ReadSmsBroadcastReceiver extends BroadcastReceiver {

    public ReadSmsBroadcastReceiverListener readSmsBroadcastReceiverListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(SmsRetriever.SMS_RETRIEVED_ACTION)) {
            Bundle extras = intent.getExtras();
            Status smsRetrieverStatus = null;
            if (extras != null) {
                smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
            }
            if (smsRetrieverStatus != null) {
                switch (smsRetrieverStatus.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        Intent messageIntent = new Intent();
                        messageIntent.putExtra("message", extras.get(SmsRetriever.EXTRA_SMS_MESSAGE).toString().trim());
                        if (readSmsBroadcastReceiverListener != null) {
                            readSmsBroadcastReceiverListener.onSuccess(messageIntent);
                        }
                        break;
                    case CommonStatusCodes.TIMEOUT:
                        if (readSmsBroadcastReceiverListener != null) {
                            readSmsBroadcastReceiverListener.onFailure();
                        }
                        break;
                }
            }
        }
    }

    public interface ReadSmsBroadcastReceiverListener {

        void onSuccess(Intent intent);

        void onFailure();
    }
}
