package com.helpinghands.helperstations.authorization.login.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReqGetGoogleAuth {

    @SerializedName("emailAddress")
    @Expose
    private String emailAddress;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
