package com.helpinghands.helperstations.notifications.activity;

import android.app.Activity;
import android.content.Context;

import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerListener;
import com.helpinghands.helperstations.notifications.adapter.NotificationListAdapter;
import com.helpinghands.helperstations.notifications.model.NotificationModel;

public class NotificationsActivity extends BaseActivity implements OKRecyclerListener<NotificationModel> {


    @Override
    public int setLayout() {
        return R.layout.activity_notifications;
    }

    @Override
    public Activity getCurrentActivity() {
        return NotificationsActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return NotificationsActivity.this;
    }

    @Override
    public void initView() {

        initToolBar(false, true);
        initializeOkRecyclerView(false, getCurrentContext(), getParentView(), true, 0);
    }

    @Override
    public void startCode() {
        NotificationListAdapter notificationListAdapter = new NotificationListAdapter(NotificationsActivity.this);

        NotificationModel notificationModel1 = new NotificationModel();
        notificationModel1.setNotificationTitle("Get $50 on your first 3 help ");
        notificationModel1.setNotificationDateTime("Today 6:00 PM");
        notificationModel1.setNotificationDetails("You will get $50 on your first 3 help on help station. Be online to help station and have a praise.");

        NotificationModel notificationModel2 = new NotificationModel();
        notificationModel2.setNotificationTitle("Congratulations. You got $20 on your wallet.");
        notificationModel2.setNotificationDateTime("Today 9:33 AM");
        notificationModel2.setNotificationDetails("You have successfully helped to Mr.Kaushal Oza. We appreciate your kind help and we are glad to have a kind people like you.");

        NotificationModel notificationModel3 = new NotificationModel();
        notificationModel3.setNotificationTitle("Get $50 on your first 3 help ");
        notificationModel3.setNotificationDateTime("Today 6:00 PM");
        notificationModel3.setNotificationDetails("You will get $50 on your first 3 help on help station. Be online to help station and have a praise.");

        NotificationModel notificationModel4 = new NotificationModel();
        notificationModel4.setNotificationTitle("Congratulations. You got $20 on your wallet.");
        notificationModel4.setNotificationDateTime("Today 9:33 AM");
        notificationModel4.setNotificationDetails("You have successfully helped to Mr.Kaushal Oza. We appreciate your kind help and we are glad to have a kind people like you.");


        notificationListAdapter.addItem(notificationModel1);
        notificationListAdapter.addItem(notificationModel2);
        notificationListAdapter.addItem(notificationModel3);
        notificationListAdapter.addItem(notificationModel4);

        okRecyclerView.setAdapter(notificationListAdapter);
        notificationListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRetroSuccess(String method, Object object) {


    }

    @Override
    public void onRetroFailure(String method, Object object) {


    }

    @Override
    public String setToolbarTitle() {
        return "Notifications";
    }

    @Override
    public void showEmptyDataView(int resId) {
        okRecyclerView.showEmptyDataView(getString(resId));
    }

    @Override
    public void onRecyclerItemClick(int position, NotificationModel item) {

    }

    @Override
    public void onSwipeRefresh() {
        super.onSwipeRefresh();
        swipeRefreshLayout.setRefreshing(false);
    }
}
