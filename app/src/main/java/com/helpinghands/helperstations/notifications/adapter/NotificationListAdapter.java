package com.helpinghands.helperstations.notifications.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerListener;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerViewAdapter;
import com.helpinghands.helperstations.notifications.model.NotificationModel;

import java.util.ArrayList;

public class NotificationListAdapter extends OKRecyclerViewAdapter<NotificationModel, OKRecyclerListener<NotificationModel>> {


    public NotificationListAdapter(OKRecyclerListener<NotificationModel> listener) {
        super(listener);
    }

    @Override
    public void onBindData(RecyclerView.ViewHolder holder, NotificationModel val, int position) {
        NotificationViewHolder notificationViewHolder = (NotificationViewHolder) holder;
        notificationViewHolder.tvNotificationTitle.setText(val.getNotificationTitle());
        notificationViewHolder.tvNotificationDetails.setText(val.getNotificationDetails());
        notificationViewHolder.tvNotificationDate.setText(val.getNotificationDateTime());
    }

    @Override
    public RecyclerView.ViewHolder onBindViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_list, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public ArrayList<String> compareFieldValue(NotificationModel item, ArrayList<String> searchItemList) {
        return null;
    }

    private static class NotificationViewHolder extends RecyclerView.ViewHolder {

        private TextView tvNotificationTitle;
        private TextView tvNotificationDate;
        private TextView tvNotificationDetails;

        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNotificationTitle = itemView.findViewById(R.id.tvNotificationTitle);
            tvNotificationDate = itemView.findViewById(R.id.tvNotificationDate);
            tvNotificationDetails = itemView.findViewById(R.id.tvNotificationDetails);
        }
    }

}
