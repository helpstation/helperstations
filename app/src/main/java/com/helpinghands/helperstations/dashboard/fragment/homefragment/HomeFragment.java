package com.helpinghands.helperstations.dashboard.fragment.homefragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import androidx.cardview.widget.CardView;

import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.dashboard.activitiy.DashboardActivity;
import com.helpinghands.helperstations.dashboard.fragment.HelpMeFragment;
import com.helpinghands.helperstations.hmutils.base.BaseFragment;

import static com.helpinghands.helperstations.hmutils.utils.EventsHandler.event_home_to_help_me_fragment;

public class HomeFragment extends BaseFragment {

    private ViewFlipper viewFlipper;
    private RelativeLayout rLEmergencyConnect;
    private RelativeLayout rLHelpMe;
    private RelativeLayout rLMyEarns;
    private RelativeLayout rLYourHelperBuddy;

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public Activity getCurrentActivity() {
        return getActivity();
    }

    @Override
    public Context getCurrentContext() {
        return HomeFragment.this.getContext();
    }

    @Override
    public void initView() {

        rLEmergencyConnect = view.findViewById(R.id.rLEmergencyConnect);
        rLHelpMe = view.findViewById(R.id.rLHelpMe);
        rLHelpMe.setOnClickListener(this);
        rLMyEarns = view.findViewById(R.id.rLMyEarns);
        rLYourHelperBuddy = view.findViewById(R.id.rLYourHelperBuddy);
    }

    @Override
    public void startCode() {

        viewFlipper = view.findViewById(R.id.viewFlipper);

        // Setting IN and OUT animation for view flipper
        viewFlipper.setInAnimation(getCurrentContext(), R.anim.right_enter);
        viewFlipper.setOutAnimation(getCurrentContext(), R.anim.left_out);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.startFlipping();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rLHelpMe:
                ((DashboardActivity)getActivity()).onEventFire(HomeFragment.class, event_home_to_help_me_fragment, null);
                break;
        }
    }

    @Override
    public String setToolbarTitle() {
        return "Home";
    }
}