package com.helpinghands.helperstations.dashboard.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.BaseFragment;
import com.helpinghands.helperstations.hmutils.preferences.LocationData;

public class LocationFragment extends BaseFragment implements OnMapReadyCallback {

    private MapView mMapView;

    public LocationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_location;
    }

    @Override
    public Activity getCurrentActivity() {
        return getActivity();
    }

    @Override
    public Context getCurrentContext() {
        return LocationFragment.this.getContext();
    }

    @Override
    public void initView() {
        mMapView = view.findViewById(R.id.map);
    }

    @Override
    public void startCode() {

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        try {
            MapsInitializer.initialize(getCurrentContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        @SuppressLint("ResourceType") View zoomControls = mMapView.findViewById(0x1);

        if (zoomControls != null && zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            // ZoomControl is inside of RelativeLayout
            RelativeLayout.LayoutParams params_zoom = (RelativeLayout.LayoutParams) zoomControls.getLayoutParams();

            // Align it to - parent top|left
            params_zoom.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params_zoom.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            // Update margins, set to 10dp
            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
            params_zoom.setMargins(0, 0, 0, 40);
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                LocationData.getInstance().googleMap = map;

                if (ActivityCompat.checkSelfPermission(getCurrentActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getCurrentActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                LocationData.getInstance().googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                LocationData.getInstance().googleMap.setMyLocationEnabled(true);
                LocationData.getInstance().googleMap.isIndoorEnabled();
                LocationData.getInstance().googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        LocationData.getInstance().googleMap.clear();
                        LatLng currentLatLong = new LatLng(LocationData.getInstance().currentlatitude, LocationData.getInstance().currentlongitude);
                        LocationData.getInstance().googleMap.addMarker(new MarkerOptions().position(currentLatLong).title("You are here."));
                        LocationData.getInstance().googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 15));
                        return false;
                    }
                });
                LocationData.getInstance().googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                LocationData.getInstance().googleMap.getUiSettings().setCompassEnabled(true);
                LocationData.getInstance().googleMap.getUiSettings().setAllGesturesEnabled(true);
                LocationData.getInstance().googleMap.getUiSettings().setZoomControlsEnabled(true);
                LocationData.getInstance().googleMap.setBuildingsEnabled(true);
                LocationData.getInstance().googleMap.getUiSettings().setZoomGesturesEnabled(true);

                if (LocationData.getInstance().googleMap != null) {
                    LatLng currentLatLong = new LatLng(LocationData.getInstance().currentlatitude, LocationData.getInstance().currentlongitude);
                    LocationData.getInstance().googleMap.addMarker(new MarkerOptions().position(currentLatLong).title("You are here."));
                    LocationData.getInstance().googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 15));
                }
                getCurrentLocationAndAddress(LocationData.getInstance().currentLocation);
            }
        });
    }

    @Override
    public String setToolbarTitle() {
        return "Track Me";
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView != null) {
            mMapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        if (ActivityCompat.checkSelfPermission(getCurrentActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getCurrentActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationData.getInstance().googleMap = map;
        LocationData.getInstance().googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        LocationData.getInstance().googleMap.setMyLocationEnabled(true);
        LocationData.getInstance().googleMap.isIndoorEnabled();
        LocationData.getInstance().googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                LocationData.getInstance().googleMap.clear();
                return false;
            }
        });
        LocationData.getInstance().googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        LocationData.getInstance().googleMap.getUiSettings().setCompassEnabled(true);
        LocationData.getInstance().googleMap.getUiSettings().setAllGesturesEnabled(true);
        LocationData.getInstance().googleMap.getUiSettings().setZoomControlsEnabled(true);
        LocationData.getInstance().googleMap.setBuildingsEnabled(true);
        LocationData.getInstance().googleMap.getUiSettings().setZoomGesturesEnabled(true);

        if (LocationData.getInstance().googleMap != null) {
            LatLng currentLatLong = new LatLng(LocationData.getInstance().currentlatitude, LocationData.getInstance().currentlongitude);
            LocationData.getInstance().googleMap.addMarker(new MarkerOptions().position(currentLatLong).title("You are here."));
            LocationData.getInstance().googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 15));
        }
        getCurrentLocationAndAddress(LocationData.getInstance().currentLocation);
    }
}