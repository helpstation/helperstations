package com.helpinghands.helperstations.dashboard.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.dashboard.activitiy.DashboardActivity;
import com.helpinghands.helperstations.hmutils.base.BaseFragment;

import static com.helpinghands.helperstations.hmutils.utils.EventsHandler.event_help_me_to_track_location;

public class HelpMeFragment extends BaseFragment {

    private TextInputLayout tilCategory;
    private TextInputLayout tilSubCategory;
    private TextInputLayout tilDescription;
    private TextInputLayout tilYourLocation;
    private TextInputLayout tilLostItemLocation;
    private TextInputLayout tilDateOfLost;
    private TextInputLayout tilTimeOfLost;
    private TextInputLayout tilNearByPlace;
    private TextInputLayout tilFamousSpotNearer;
    private TextView textView;
    private ImageView ivAttachments;
    private Button btnHelpMe;

    public HelpMeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_helpme;
    }

    @Override
    public Activity getCurrentActivity() {
        return getActivity();
    }

    @Override
    public Context getCurrentContext() {
        return HelpMeFragment.this.getContext();
    }

    @Override
    public void initView() {

        tilCategory = view.findViewById(R.id.tilCategory);
        tilSubCategory = view.findViewById(R.id.tilSubCategory);
        tilDescription = view.findViewById(R.id.tilDescription);
        tilYourLocation = view.findViewById(R.id.tilYourLocation);
        tilLostItemLocation = view.findViewById(R.id.tilLostItemLocation);
        tilDateOfLost = view.findViewById(R.id.tilDateOfLost);
        tilTimeOfLost = view.findViewById(R.id.tilTimeOfLost);
        tilNearByPlace = view.findViewById(R.id.tilNearByPlace);
        tilFamousSpotNearer = view.findViewById(R.id.tilFamousSpotNearer);
        textView = view.findViewById(R.id.textView);
        ivAttachments = view.findViewById(R.id.ivAttachments);
        btnHelpMe = view.findViewById(R.id.btnHelpMe);
        btnHelpMe.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnHelpMe:
                ((DashboardActivity)getActivity()).onEventFire(HelpMeFragment.class, event_help_me_to_track_location, null);
                break;
        }
    }

    @Override
    public void startCode() {

    }

    @Override
    public String setToolbarTitle() {
        return "Help Me";
    }
}