package com.helpinghands.helperstations.dashboard.activitiy;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.authorization.signup.activity.SignUpMobileActivity;
import com.helpinghands.helperstations.dashboard.fragment.CollectionFragment;
import com.helpinghands.helperstations.dashboard.fragment.HelpMeFragment;
import com.helpinghands.helperstations.dashboard.fragment.LocationFragment;
import com.helpinghands.helperstations.dashboard.fragment.MiscItemsFragment;
import com.helpinghands.helperstations.dashboard.fragment.OwnedItemsFragment;
import com.helpinghands.helperstations.dashboard.fragment.WantToHelpFragment;
import com.helpinghands.helperstations.dashboard.fragment.homefragment.HomeFragment;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.hmutils.preferences.LocationData;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;
import com.helpinghands.helperstations.hmutils.utils.EventsHandler;
import com.helpinghands.helperstations.notifications.activity.NotificationsActivity;
import com.helpinghands.helperstations.profile.activity.ProfileActivity;
import com.helpinghands.helperstations.settings.activity.SettingActivity;

import java.util.Timer;
import java.util.TimerTask;

public class DashboardActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    // change END_SCALE value if you want to show animation to main contentview as small layout
    private static final float END_SCALE = 1f;
    private final int POS_HOME_FRAGMENT = 0;
    private final int POS_HELPME_FRAGMENT = 1;
    private final int POS_WANTTOHELP_FRAGMENT = 2;
    private final int POS_COLLECTION_FRAGMENT = 3;
    private final int POS_OWNEDITEMS_FRAGMENT = 4;
    private final int POS_MISCITEMS_FRAGMENT = 5;
    private final int POS_LOCATION_FRAGMENT = 6;
    private final int POS_SETTINGS_FRAGMENT = 7;
    public NavigationView navigationView;
    private int currentPos = 0;
    private Fragment currentFragment;
    private LinearLayout mainLinearLayout;
    private boolean doubleBackToExitPressedOnce = false;
    private TextView tvNotificationItemCount, tvCurrentLocation;
    private ProgressBar prgFetchLocation;
    private Timer addressFetchTimer = new Timer();

    @Override
    public int setLayout() {
        return R.layout.activity_dashboard;
    }

    @Override
    public Activity getCurrentActivity() {
        return DashboardActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return DashboardActivity.this;
    }

    @Override
    public void initView() {
        initToolBar(true, false);

        mainLinearLayout = findViewById(R.id.mainLinearLayout);
        DrawerLayout drawerLayout = findViewById(R.id.parentView);
        navigationView = findViewById(R.id.nav_view);

        ActionBarDrawerToggle toggle =
                new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        View myheaderview = navigationView.getHeaderView(0);
        myheaderview.setOnClickListener(this);

        NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
        if (navigationMenuView != null) {
            ImageView ivUserProfile = myheaderview.findViewById(R.id.ivUserProfile);
            TextView tvUsername = myheaderview.findViewById(R.id.tvUsername);
            TextView tvMobileNumber = myheaderview.findViewById(R.id.tvMobileNumber);
            TextView tvEmailAddress = myheaderview.findViewById(R.id.tvEmailAddress);
            tvCurrentLocation = myheaderview.findViewById(R.id.tvCurrentLocation);
            prgFetchLocation = myheaderview.findViewById(R.id.prgFetchLocation);
            tvUsername.setText(Preferences.getPreference().getString(getCurrentActivity(), R.string.pref_api_param_username));
            tvMobileNumber.setText(Preferences.getPreference().getString(getCurrentActivity(), R.string.pref_api_param_mobile_number));
            tvEmailAddress.setText(Preferences.getPreference().getString(getCurrentActivity(), R.string.pref_api_param_email_address));

            // Set user profile image
            try {
                if (!TextUtils.isEmpty(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_profile_pic_path))) {
                    CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getCurrentContext());
                    circularProgressDrawable.setStrokeWidth(5f);
                    circularProgressDrawable.setCenterRadius(30f);
                    circularProgressDrawable.start();
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.circleCrop();
                    requestOptions.placeholder(circularProgressDrawable);
                    requestOptions.error(android.R.drawable.stat_notify_error);

                    Glide.with(getCurrentContext())
                            .applyDefaultRequestOptions(requestOptions)
                            .load(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_profile_pic_path))
                            .into(ivUserProfile);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (TextUtils.isEmpty(LocationData.getInstance().currentLocationAddress)) {
                    prgFetchLocation.setVisibility(View.VISIBLE);
                } else {
                    prgFetchLocation.setVisibility(View.GONE);
                }
                tvCurrentLocation.setText(LocationData.getInstance().currentLocationAddress);
                tvCurrentLocation.setSelected(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        addressFetchTimer = new Timer();
        addressFetchTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            getCurrentLocationAndAddress(LocationData.getInstance().currentLocation);
                            if (prgFetchLocation != null) {
                                if (TextUtils.isEmpty(LocationData.getInstance().currentLocationAddress)) {
                                    prgFetchLocation.setVisibility(View.VISIBLE);
                                } else {
                                    tvCurrentLocation.setText(LocationData.getInstance().currentLocationAddress);
                                    prgFetchLocation.setVisibility(View.GONE);
                                    addressFetchTimer.cancel();
                                    addressFetchTimer.purge();
                                    addressFetchTimer = null;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, 0, 2000);

        toolbar.setNavigationIcon(new DrawerArrowDrawable(activity));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     if (drawerLayout.isDrawerOpen(navigationView)) {
                                                         drawerLayout.closeDrawer(navigationView);
                                                     } else {
                                                         drawerLayout.openDrawer(navigationView);
                                                     }
                                                 }
                                             }
        );

        drawerLayout.setScrimColor(Color.TRANSPARENT);
        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                                           @Override
                                           public void onDrawerOpened(View drawerView) {
                                               super.onDrawerOpened(drawerView);
                                           }

                                           @Override
                                           public void onDrawerSlide(View drawerView, float slideOffset) {
                                               final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                                               final float offsetScale = 1 - diffScaledOffset;
                                               mainLinearLayout.setScaleX(offsetScale);
                                               mainLinearLayout.setScaleY(offsetScale);

                                               // Translate the View, accounting for the scaled width
                                               final float xOffset = drawerView.getWidth() * slideOffset;
                                               final float xOffsetDiff = mainLinearLayout.getWidth() * diffScaledOffset / 2;
                                               final float xTranslation = xOffset - xOffsetDiff;
                                               mainLinearLayout.setTranslationX(xTranslation);
                                           }

                                           @Override
                                           public void onDrawerClosed(View drawerView) {
                                               drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                                           }
                                       }
        );
        displayDrawer(POS_HOME_FRAGMENT);

        if (TextUtils.isEmpty(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_mobile_number))) {
            CardView cvRegisterPopup = findViewById(R.id.cvRegisterPopup);
            TextView tvRegister = findViewById(R.id.tvRegister);
            tvRegister.setOnClickListener(this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    expandViewVertical(cvRegisterPopup, 1000);
                }
            }, 1000);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void startCode() {

        if (getIntent() != null) {
            if (getIntent().hasExtra("NewUser")) {
                boolean newUser = getIntent().getBooleanExtra("NewUser", false);
                if (newUser) {
                    showSnakbar("Welcome to ".concat(getString(R.string.app_name)), false, getParentView());
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tvRegister:
                startActivityForResult(new Intent(getCurrentActivity(), SignUpMobileActivity.class).putExtra("Context", "RegisterMobile"), INTENT_RQ_RESPONSE_OTP);
                break;

            case R.id.navHeaderParentView:
                startActivity(new Intent(getCurrentActivity(), ProfileActivity.class));
                break;
        }
    }

    @Override
    public void onRetroSuccess(String method, Object object) {
        super.onRetroSuccess(method, object);

    }

    @Override
    public void onRetroFailure(String method, Object object) {
        super.onRetroFailure(method, object);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.home_menu, menu);
            MenuItem menuItem = menu.findItem(R.id.menu_item_notification);
            menuItem.setActionView(R.layout.custom_action_item_layout);
            View searchView = (View) menuItem.getActionView();
            tvNotificationItemCount = searchView.findViewById(R.id.notification_badge);
            searchView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getCurrentActivity(), NotificationsActivity.class));
                }
            });
            setupBadge(tvNotificationItemCount, 4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (turnGPSOn()) {
            getCurrentLocationAndAddress(LocationData.getInstance().currentLocation);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case INTENT_RQ_RESPONSE_OTP:
                if (resultCode == RESULT_OK && !TextUtils.isEmpty(Preferences.getPreference().getString(getCurrentContext(), R.string.edt_hint_mobile_number))) {
                    CardView cvRegisterPopup = findViewById(R.id.cvRegisterPopup);
                    collapseViewVertical(cvRegisterPopup, 1500);
                }
                break;
        }
    }

    @Override
    public String setToolbarTitle() {
        return "Help Me";
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.tab_home:
                displayDrawer(POS_HOME_FRAGMENT);
                break;
            case R.id.tab_helpme:
                displayDrawer(POS_HELPME_FRAGMENT);
                break;
            case R.id.tab_wanttohelp:
                displayDrawer(POS_WANTTOHELP_FRAGMENT);
                break;
            case R.id.tab_map:
                displayDrawer(POS_LOCATION_FRAGMENT);
                break;
            case R.id.tab_collections:
                displayDrawer(POS_COLLECTION_FRAGMENT);
                break;
            case R.id.tab_owned:
                displayDrawer(POS_OWNEDITEMS_FRAGMENT);
                break;
            case R.id.tab_misc_collections:
                displayDrawer(POS_MISCITEMS_FRAGMENT);
                break;
            case R.id.tab_settings:
                displayDrawer(POS_SETTINGS_FRAGMENT);
                break;
        }

        DrawerLayout drawer = findViewById(R.id.parentView);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /* ***********************************  SET YOUR FRAGMENT ON DRAWER CLICK HERE  ************************************* */
    private void displayDrawer(int pos) {

        try {

            switch (pos) {
                case POS_HOME_FRAGMENT:
                    currentPos = POS_HOME_FRAGMENT;
                    currentFragment = new HomeFragment();
                    break;
                case POS_HELPME_FRAGMENT:
                    currentPos = POS_HELPME_FRAGMENT;
                    currentFragment = new HelpMeFragment();
                    break;
                case POS_WANTTOHELP_FRAGMENT:
                    currentPos = POS_WANTTOHELP_FRAGMENT;
                    currentFragment = new WantToHelpFragment();
                    break;
                case POS_COLLECTION_FRAGMENT:
                    currentPos = POS_COLLECTION_FRAGMENT;
                    currentFragment = new CollectionFragment();
                    break;
                case POS_OWNEDITEMS_FRAGMENT:
                    currentPos = POS_OWNEDITEMS_FRAGMENT;
                    currentFragment = new OwnedItemsFragment();
                    break;
                case POS_MISCITEMS_FRAGMENT:
                    currentPos = POS_MISCITEMS_FRAGMENT;
                    currentFragment = new MiscItemsFragment();
                    break;
                case POS_SETTINGS_FRAGMENT:
                    startActivity(new Intent(getCurrentActivity(), SettingActivity.class));
                    break;
                case POS_LOCATION_FRAGMENT:
                    currentPos = POS_LOCATION_FRAGMENT;
                    currentFragment = new LocationFragment();
                    break;
            }

            setFragment(currentFragment);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.parentView);
        Fragment currentFragment;
        try {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {

                currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);

                if (currentFragment instanceof HomeFragment) {

                    if (!doubleBackToExitPressedOnce) {

                        this.doubleBackToExitPressedOnce = true;
                        showSnakbar("Press Back Again to Exit", false, getParentView());

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {

                                doubleBackToExitPressedOnce = false;
                            }
                        }, 2000);
                    } else {
                        finish();
                    }
                } else {
                    setFragment(new HomeFragment());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFire(Class fromClass, String event, Object o) {
        super.onEventFire(fromClass, event, o);

        switch (fromClass.getSimpleName()) {

            case "HomeFragment":
                switch (event) {
                    case EventsHandler.event_home_to_help_me_fragment:
                        displayDrawer(POS_HELPME_FRAGMENT);
                        break;
                }
                break;

            case "HelpMeFragment":
                switch (event) {
                    case EventsHandler.event_help_me_to_track_location:
                        displayDrawer(POS_LOCATION_FRAGMENT);
                        break;
                }
                break;
        }
    }
}
