package com.helpinghands.helperstations.dashboard.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.BaseFragment;
import com.helpinghands.helperstations.hmutils.preferences.LocationData;

public class CollectionFragment extends BaseFragment {

    private ImageView ivUserProfile;
    private TextView tvUsername;
    private TextView tvMobileNumber;
    private TextView tvEmailAddress;
    private TextView tvCurrentLocation;

    public CollectionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_collections;
    }

    @Override
    public Activity getCurrentActivity() {
        return getActivity();
    }

    @Override
    public Context getCurrentContext() {
        return CollectionFragment.this.getContext();
    }

    @Override
    public void initView() {

        ivUserProfile = view.findViewById(R.id.ivUserProfile);
        tvUsername = view.findViewById(R.id.tvUsername);
        tvMobileNumber = view.findViewById(R.id.tvMobileNumber);
        tvEmailAddress = view.findViewById(R.id.tvEmailAddress);
        tvCurrentLocation = view.findViewById(R.id.tvCurrentLocation);
        try {
            tvCurrentLocation.setText(LocationData.getInstance().currentLocationAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startCode() {

    }

    @Override
    public String setToolbarTitle() {
        return "Collections";
    }
}