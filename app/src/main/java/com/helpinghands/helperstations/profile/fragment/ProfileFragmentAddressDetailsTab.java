package com.helpinghands.helperstations.profile.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.BaseFragment;

public class ProfileFragmentAddressDetailsTab extends BaseFragment {

    public ProfileFragmentAddressDetailsTab() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_profile_address_details_tab;
    }

    @Override
    public Activity getCurrentActivity() {
        return getActivity();
    }

    @Override
    public Context getCurrentContext() {
        return ProfileFragmentAddressDetailsTab.this.getContext();
    }

    @Override
    public void initView() {

    }

    @Override
    public void startCode() {

    }

    @Override
    public String setToolbarTitle() {
        return "Profile";
    }
}