package com.helpinghands.helperstations.profile.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioButton;

import com.google.android.material.textfield.TextInputLayout;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.BaseFragment;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;

import java.util.Date;

public class ProfileFragmentPersonalDetailsTab extends BaseFragment {

    private TextInputLayout tilFirstname;
    private TextInputLayout tilLastname;
    private TextInputLayout tilMobileNumber;
    private TextInputLayout tilEmailAddress;
    private TextInputLayout tilBirthDate;
    private TextInputLayout tilAge;
    private RadioButton rbMale;
    private RadioButton rbFemale;
    private RadioButton rbOthers;

    public ProfileFragmentPersonalDetailsTab() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_profile_personal_details_tab;
    }

    @Override
    public Activity getCurrentActivity() {
        return getActivity();
    }

    @Override
    public Context getCurrentContext() {
        return ProfileFragmentPersonalDetailsTab.this.getContext();
    }

    @Override
    public void initView() {

        tilFirstname = view.findViewById(R.id.tilFirstname);
        tilLastname = view.findViewById(R.id.tilLastname);
        tilMobileNumber = view.findViewById(R.id.tilMobileNumber);
        tilEmailAddress = view.findViewById(R.id.tilEmailAddress);
        tilBirthDate = view.findViewById(R.id.tilBirthDate);
        tilBirthDate.setOnClickListener(this);
        tilAge = view.findViewById(R.id.tilAge);
        rbMale = view.findViewById(R.id.rbMale);
        rbFemale = view.findViewById(R.id.rbFemale);
        rbOthers = view.findViewById(R.id.rbOthers);

        tilFirstname.getEditText().setText(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_first_name));
        tilLastname.getEditText().setText(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_last_name));
        tilMobileNumber.getEditText().setText(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_mobile_number));
        tilEmailAddress.getEditText().setText(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_email_address));
        tilBirthDate.getEditText().setText(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_birth_date));

        if (!TextUtils.isEmpty(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_birth_date))) {
            AgeCalculator ageCalculator = new AgeCalculator();
            Date birthDate = stringToDate(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_birth_date), dd_MMM_yyyy_NO_DASH, false);
            AgeCalculator.Age age = ageCalculator.calculateAge(birthDate);
            tilAge.getEditText().setText(String.valueOf(age.getYears()).concat(" Years"));
        }

        if (!TextUtils.isEmpty(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_gender))) {
            if (Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_gender).equalsIgnoreCase("MALE")) {
                rbMale.setChecked(true);
            } else if (Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_gender).equalsIgnoreCase("FEMALE")) {
                rbFemale.setChecked(true);
            } else {
                rbOthers.setChecked(true);
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tilBirthDate:
                showDatePickerDialog(tilBirthDate, dd_MMM_yyyy_NO_DASH);
                break;
        }
    }

    @Override
    public void startCode() {

    }

    @Override
    public String setToolbarTitle() {
        return "Profile";
    }
}