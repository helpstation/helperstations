package com.helpinghands.helperstations.profile.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;
import com.helpinghands.helperstations.hmutils.views.OKCustomViewpager;
import com.helpinghands.helperstations.profile.fragment.ProfileFragmentAddressDetailsTab;
import com.helpinghands.helperstations.profile.fragment.ProfileFragmentPersonalDetailsTab;
import com.helpinghands.helperstations.profile.fragment.ProfileFragmentSocialDetailsTab;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProfileActivity extends BaseActivity {

    private NestedScrollView nsProfile;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView ivBackArrow;
    private ImageView ivUserProfile;
    private OKCustomViewpager okCustomViewpager;
    private TabLayout tabLayout;
    private TextView tvUsername;
    private TextView tvBio;
    private TextView tvEmailAddress;

    public ProfileActivity() {
    }

    @Override
    public int setLayout() {
        return R.layout.activity_profile;
    }

    @Override
    public Activity getCurrentActivity() {
        return ProfileActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return ProfileActivity.this;
    }

    @Override
    public void initView() {

        okCustomViewpager = findViewById(R.id.okCustomViewpager);
        tabLayout = findViewById(R.id.tabLayout);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(this);
        ivUserProfile = findViewById(R.id.ivUserProfile);
        ivUserProfile.setOnClickListener(this);
        nsProfile = findViewById(R.id.nsProfile);
        tvUsername = findViewById(R.id.tvUsername);
        tvBio = findViewById(R.id.tvBio);
        tvEmailAddress = findViewById(R.id.tvEmailAddress);

        collapsingToolbarLayout = findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setTitle("Profile");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Profile");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });

        nsProfile.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onGlobalLayout() {

                final int scrollViewHeight = nsProfile.getHeight();

                if (scrollViewHeight > 0) {

                    nsProfile.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    final View lastView = nsProfile.getChildAt(nsProfile.getChildCount() - 1);
                    final int lastViewBottom = lastView.getBottom() + nsProfile.getPaddingBottom();
                    final int deltaScrollY = lastViewBottom - scrollViewHeight - nsProfile.getScrollY();
                    nsProfile.smoothScrollBy(0, deltaScrollY);
                }
            }
        });

        setupViewPager();

        TextView tvUsername = findViewById(R.id.tvUsername);
        tvUsername.setText(Preferences.getPreference().getString(getCurrentActivity(), R.string.pref_api_param_username));

        if (!TextUtils.isEmpty(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_birth_date))) {
            AgeCalculator ageCalculator = new AgeCalculator();
            Date birthDate = stringToDate(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_birth_date), dd_MMM_yyyy_NO_DASH, false);
            AgeCalculator.Age age = ageCalculator.calculateAge(birthDate);
            tvBio.setText(String.valueOf(age.getYears()).concat(" Years, ").concat(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_gender)));
        }

        // Set user profile image
        try {
            if (!TextUtils.isEmpty(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_profile_pic_path))) {
                CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getCurrentContext());
                circularProgressDrawable.setStrokeWidth(5f);
                circularProgressDrawable.setCenterRadius(30f);
                circularProgressDrawable.start();

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.circleCrop();
                requestOptions.placeholder(circularProgressDrawable);
                requestOptions.error(android.R.drawable.stat_notify_error);

                Glide.with(getCurrentContext())
                        .applyDefaultRequestOptions(requestOptions)
                        .load(Preferences.getPreference().getString(getCurrentContext(), R.string.pref_api_param_profile_pic_path))
                        .into(ivUserProfile);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupViewPager() {

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @SuppressLint("RestrictedApi")
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                try {
                    switch (tab.getPosition()) {

                        case 0:
                            okCustomViewpager.setCurrentItem(0);
                            break;

                        case 1:
                            okCustomViewpager.setCurrentItem(1);
                            break;

                        case 2:
                            okCustomViewpager.setCurrentItem(2);
                            break;

                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        okCustomViewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        okCustomViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onPageSelected(int position) {

                try {
                    switch (position) {

                        case 0:
                            okCustomViewpager.setCurrentItem(0);
                            break;

                        case 1:
                            okCustomViewpager.setCurrentItem(1);
                            break;

                        case 2:
                            okCustomViewpager.setCurrentItem(2);
                            break;

                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        ProfileFragmentPersonalDetailsTab profileFragmentPersonalDetailsTab = new ProfileFragmentPersonalDetailsTab();
        ProfileFragmentSocialDetailsTab profileFragmentSocialDetailsTab = new ProfileFragmentSocialDetailsTab();
        ProfileFragmentAddressDetailsTab profileFragmentAddressDetailsTab = new ProfileFragmentAddressDetailsTab();

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(profileFragmentPersonalDetailsTab);
        viewPagerAdapter.addFragment(profileFragmentSocialDetailsTab);
        viewPagerAdapter.addFragment(profileFragmentAddressDetailsTab);
        okCustomViewpager.setOffscreenPageLimit(3);
        okCustomViewpager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.ivBackArrow:
                onBackPressed();
                break;
        }
    }

    @Override
    public void startCode() {

    }

    @Override
    public String setToolbarTitle() {
        return "Profile";
    }

    public static class ViewPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragmentList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        void addFragment(Fragment fragment) {
            fragmentList.add(fragment);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}