package com.helpinghands.helperstations.settings.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.helpinghands.helperstations.BuildConfig;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;
import com.helpinghands.helperstations.hmutils.views.OKAlertDialog;

public class SettingActivity extends BaseActivity {

    private TextView tvTermsOfService;
    private TextView tvPrivacyPolicy;
    private TextView tvContactUs;
    private TextView tvChangePassword;
    private TextView tvSignOut;
    private TextView tvVersion;
    private ImageView ivAppLogo;
    private Switch swFingerPrint;

    @Override
    public int setLayout() {
        return R.layout.activity_setting;
    }

    @Override
    public Activity getCurrentActivity() {
        return SettingActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return SettingActivity.this;
    }

    @Override
    public void initView() {
        initToolBar(false, true);
        tvTermsOfService = findViewById(R.id.tvTermsOfService);
        tvTermsOfService.setOnClickListener(this);
        tvPrivacyPolicy = findViewById(R.id.tvPrivacyPolicy);
        tvPrivacyPolicy.setOnClickListener(this);
        tvContactUs = findViewById(R.id.tvContactUs);
        tvContactUs.setOnClickListener(this);
        tvChangePassword = findViewById(R.id.tvChangePassword);
        tvChangePassword.setOnClickListener(this);
        swFingerPrint = findViewById(R.id.swFingerPrint);
        tvSignOut = findViewById(R.id.tvSignOut);
        tvSignOut.setOnClickListener(this);
        ivAppLogo = findViewById(R.id.ivAppLogo);
        tvVersion = findViewById(R.id.tvVersion);
        tvVersion.setText(BuildConfig.VERSION_NAME);

        swFingerPrint.setChecked(Preferences.getPreference().getBoolean(getCurrentContext(), R.string.pref_settings_use_biometric_login));
        swFingerPrint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (checkBiometricSupport()) {
                    Preferences.getPreference().addBoolean(getCurrentContext(), R.string.pref_settings_use_biometric_login, b);
                } else {
                    swFingerPrint.setChecked(false);
                    Preferences.getPreference().addBoolean(getCurrentContext(), R.string.pref_settings_use_biometric_login, false);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.tvSignOut:
                new OKAlertDialog(activity) {
                    @Override
                    public boolean setCancelable() {
                        return true;
                    }

                    @Override
                    public String setTitle() {
                        return "Logout?";
                    }

                    @Override
                    public String setMessage() {
                        return "Are you sure you want to logout from current user ?";
                    }

                    @Override
                    public Drawable setIcon() {
                        return null;
                    }

                    @Override
                    public String setPositiveButtonText() {
                        return "Logout";
                    }

                    @Override
                    public DialogInterface.OnClickListener setPositiveButtonClick() {
                        return (dialog, which) -> {
                            dialog.dismiss();
                            makeLogout();
                        };
                    }

                    @Override
                    public String setNegativeButtonText() {
                        return "Cancel";
                    }

                    @Override
                    public DialogInterface.OnClickListener setNegativeButtonClick() {
                        return (dialog, which) -> {
                            dialog.dismiss();
                        };
                    }

                    @Override
                    public String setNeutralButtonText() {
                        return null;
                    }

                    @Override
                    public DialogInterface.OnClickListener setNeutralButtonClick() {
                        return null;
                    }

                    @Override
                    public boolean setAutoShow() {
                        return true;
                    }

                    @Override
                    public DialogInterface.OnDismissListener setOnDismissListener() {
                        return null;
                    }
                };
                break;
        }
    }

    @Override
    public void startCode() {

    }

    @Override
    public String setToolbarTitle() {
        return "Settings";
    }
}
