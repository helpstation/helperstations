package com.helpinghands.helperstations;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.biometrics.BiometricPrompt;
import android.os.CancellationSignal;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.helpinghands.helperstations.authorization.login.activity.LoginActivity;
import com.helpinghands.helperstations.dashboard.activitiy.DashboardActivity;
import com.helpinghands.helperstations.hmutils.base.BaseActivity;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;
import com.helpinghands.helperstations.hmutils.utils.AppSignatureHelper;

import java.util.ArrayList;

public class SplashActivity extends BaseActivity {

    /*    private final String KEY_NAME = "HelperStations";
        private RadarScanView radarScanView;
        private FingerprintManager fingerprintManager;
        private KeyStore keyStore;
        private KeyGenerator keyGenerator;
        private FingerprintManager.CryptoObject cryptoObject;*/
    private RelativeLayout relativeFingerPrintAuth;
    private ImageView ivLogo;

    private CancellationSignal cancellationSignal;

    @Override
    public int setLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public Activity getCurrentActivity() {
        return SplashActivity.this;
    }

    @Override
    public Context getCurrentContext() {
        return SplashActivity.this;
    }

    @Override
    public void initView() {
        relativeFingerPrintAuth = findViewById(R.id.relativeFingerPrintAuth);
        ivLogo = findViewById(R.id.ivLogo);
    }

    @Override
    public void startCode() {

        AppSignatureHelper appSignatureHelper = new AppSignatureHelper(getCurrentContext());
        ArrayList<String> strings = appSignatureHelper.getAppSignatures();

        if (Preferences.getPreference().getBoolean(getCurrentContext(), R.string.pref_settings_use_biometric_login)) {
//            if (checkFinger()) {
//                animateLogo(true);
//                setFingerPrint();
//            }
            if (checkBiometricSupport()) {
                authenticateUser();
            }
            return;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setNavigate();
            }
        }, 2000);
    }

    private void setNavigate() {
        if (Preferences.getPreference().getBoolean(getCurrentContext(), R.string.pref_userLoggedIn)) {
            startActivity(new Intent(getCurrentContext(), DashboardActivity.class));
            finish();
        } else {
            startActivity(new Intent(getCurrentContext(), LoginActivity.class));
            finish();
        }
    }

    public void authenticateUser() {
        BiometricPrompt biometricPrompt = new BiometricPrompt.Builder(this)
                .setTitle("Biometric Authentication")
                .setSubtitle("Authentication is required to continue")
                .setDescription("We use biometric authentication to protect your data.")
                .setNegativeButton("Cancel", this.getMainExecutor(),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        })
                .build();

        biometricPrompt.authenticate(getCancellationSignal(), getMainExecutor(), getAuthenticationCallback());
    }

    private CancellationSignal getCancellationSignal() {

        cancellationSignal = new CancellationSignal();
        cancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener() {
            @Override
            public void onCancel() {
                showSnakbar("Cancelled via signal", true, getParentView());
            }
        });
        return cancellationSignal;
    }

    private BiometricPrompt.AuthenticationCallback getAuthenticationCallback() {

        return new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, CharSequence errString) {
                showSnakbar("Authentication error: " + errString, true, getParentView());
                super.onAuthenticationError(errorCode, errString);
            }

            @Override
            public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                super.onAuthenticationHelp(helpCode, helpString);
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
            }

            @Override
            public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                setNavigate();
            }
        };
    }

    @Override
    public void onRetroSuccess(String method, Object object) {


    }

    @Override
    public void onRetroFailure(String method, Object object) {


    }

    @Override
    public String setToolbarTitle() {
        return "";
    }

    /*  // Todo : Old FingerPrint

    private void animateLogo(boolean enable) {

        if (enable)
            expandViewVertical(relativeFingerPrintAuth, 1500);
        else
            collapseViewVertical(relativeFingerPrintAuth, 1500);

        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(ivLogo, "scaleX", enable ? 0.7f : 1f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(ivLogo, "scaleY", enable ? 0.7f : 1f);
        scaleDownX.setDuration(1500);
        scaleDownY.setDuration(1500);

        ObjectAnimator moveUpY = ObjectAnimator.ofFloat(ivLogo, "translationY", enable ? -100 : 100);
        moveUpY.setDuration(1500);

        AnimatorSet scaleDown = new AnimatorSet();
        AnimatorSet moveUp = new AnimatorSet();

        scaleDown.play(scaleDownX).with(scaleDownY);
        moveUp.play(moveUpY);

        scaleDown.start();
        moveUp.start();
    }

    public boolean checkFinger() {

        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

                fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                if (fingerprintManager != null && !fingerprintManager.isHardwareDetected()) {
                    Toast.makeText(getCurrentContext(), getResources().getString(R.string.app_name), Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (fingerprintManager != null && !fingerprintManager.hasEnrolledFingerprints()) {

                    new OKAlertDialog(getCurrentContext()) {

                        @Override
                        public boolean setCancelable() {
                            return true;
                        }

                        @Override
                        public String setTitle() {
                            return getResources().getString(R.string.not_configured_fingure);
                        }

                        @Override
                        public String setMessage() {
                            return "Do you want to configure your finger to device ?";
                        }

                        @Override
                        public Drawable setIcon() {
                            return getResources().getDrawable(R.drawable.ic_fingerprint);
                        }

                        @Override
                        public String setPositiveButtonText() {
                            return "Yes";
                        }

                        @Override
                        public DialogInterface.OnClickListener setPositiveButtonClick() {
                            return (dialog, which) -> {
                                dialog.dismiss();
                                startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                            };
                        }

                        @Override
                        public String setNegativeButtonText() {
                            return "NO";
                        }

                        @Override
                        public DialogInterface.OnClickListener setNegativeButtonClick() {
                            return (dialog, which) -> finish();
                        }

                        @Override
                        public String setNeutralButtonText() {
                            return null;
                        }

                        @Override
                        public DialogInterface.OnClickListener setNeutralButtonClick() {
                            return null;
                        }

                        @Override
                        public boolean setAutoShow() {
                            return true;
                        }

                        @Override
                        public DialogInterface.OnDismissListener setOnDismissListener() {
                            return null;
                        }
                    };
                    return false;
                }

                if (keyguardManager != null && !keyguardManager.isKeyguardSecure()) {
                    Toast.makeText(getCurrentContext(), getResources().getString(R.string.not_lockscreen_enabled), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void setFingerPrint() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            radarScanView = findViewById(R.id.radarview);

            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            FingerPrintHandler fph = new FingerPrintHandler();

            try {
                generateKey();
                Cipher cipher = generateCipher();
                cryptoObject = new FingerprintManager.CryptoObject(cipher);
            } catch (Exception e) {
                e.printStackTrace();
                Log.v(TAG, e.getLocalizedMessage());
                radarScanView.setErrorRed();
            }

            fph.doAuth(fingerprintManager, cryptoObject);
        }
    }

    private void generateKey() throws Exception {

        try {

            // Get the reference to the key store
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            // Key generator to generate the key
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
            }

            keyStore.load(null);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator.init(new
                        KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_ENCRYPT |
                                KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(
                                KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
            }
            keyGenerator.generateKey();

        } catch (KeyStoreException | NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException | CertificateException | IOException exc) {
            exc.printStackTrace();
            throw new Exception(exc);
        }
    }

    private Cipher generateCipher() throws Exception {

        try {

            Cipher cipher = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
            }
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            assert cipher != null;
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return cipher;

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | UnrecoverableKeyException | KeyStoreException exc) {
            exc.printStackTrace();
            throw new Exception(exc);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public class FingerPrintHandler extends FingerprintManager.AuthenticationCallback {

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            super.onAuthenticationError(errorCode, errString);

            radarScanView.setErrorRed();

            new Handler().postDelayed(() -> radarScanView.startRadar(), 1000);
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            super.onAuthenticationHelp(helpCode, helpString);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

            super.onAuthenticationSucceeded(result);
            radarScanView.stopRadarGreen();
            animateLogo(false);
        }

        @Override
        public void onAuthenticationFailed() {
            super.onAuthenticationFailed();
            radarScanView.setErrorRed();
            new Handler().postDelayed(() -> radarScanView.startRadar(), 1000);
        }

        void doAuth(FingerprintManager manager, FingerprintManager.CryptoObject obj) {

            CancellationSignal signal = new CancellationSignal();
            try {
                manager.authenticate(obj, signal, 0, this, null);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

     */
}
