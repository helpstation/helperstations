package com.helpinghands.helperstations.hmutils.base.listener;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.authorization.login.retrofit.ReqGetGoogleAuth;
import com.helpinghands.helperstations.authorization.login.retrofit.ReqOauthTokenLogin;
import com.helpinghands.helperstations.authorization.login.retrofit.ReqUserDetails;
import com.helpinghands.helperstations.authorization.login.retrofit.ResGetGoogleAuth;
import com.helpinghands.helperstations.authorization.login.retrofit.ResOauthTokenLogin;
import com.helpinghands.helperstations.authorization.login.retrofit.ResUserDetails;
import com.helpinghands.helperstations.authorization.resetuserauth.retrofit.ReqForgotUpdatePassword;
import com.helpinghands.helperstations.authorization.resetuserauth.retrofit.ResForgotUpdatePassword;
import com.helpinghands.helperstations.authorization.signup.retrofit.ReqCheckContactExists;
import com.helpinghands.helperstations.authorization.signup.retrofit.ReqUserCreate;
import com.helpinghands.helperstations.authorization.signup.retrofit.ResCheckContactExists;
import com.helpinghands.helperstations.authorization.signup.retrofit.ResUserCreate;
import com.helpinghands.helperstations.hmutils.retrofit.RetrofitApiClient;
import com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI;
import com.helpinghands.helperstations.hmutils.views.OKAlertDialog;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.helpinghands.helperstations.hmutils.base.BaseFragment.isNeedRetroCallback;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_checkContactExists;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_oauthToken;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_profileUserDetail;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_userCreate;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_userGoogleSignUp;
import static com.helpinghands.helperstations.hmutils.retrofit.WebServicesAPI.api_userUpdatePassword;

public interface BaseApiCallInterface extends BaseDateOperationsListener {

    String API_STATUS_SUCCESS = "success";
    String API_STATUS_FAILED = "failed";
    String serialized_errorMessage_key = "error";

    default boolean isConnected(Activity activity, boolean showDialog, boolean finishActivity) {

        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = null;
        if (connectivityManager != null) {
            wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        }
        NetworkInfo mobile = null;
        if (connectivityManager != null) {
            mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        }
        if (wifi != null) {
            connected = (wifi.isAvailable() && wifi.isConnectedOrConnecting() || (mobile.isAvailable() && mobile.isConnectedOrConnecting()));
        }

        if (showDialog) {

            if (!connected) {

                new OKAlertDialog(activity) {

                    @Override
                    public boolean setCancelable() {
                        return false;
                    }

                    @Override
                    public String setTitle() {
                        return activity.getResources().getString(R.string.internetConnectionNotAvailTitle);
                    }

                    @Override
                    public String setMessage() {
                        return activity.getResources().getString(R.string.internetConnectionNotAvailMessage);
                    }

                    @Override
                    public Drawable setIcon() {
                        return activity.getResources().getDrawable(R.drawable.ic_launcher_background);
                    }

                    @Override
                    public String setPositiveButtonText() {
                        return "Retry";
                    }

                    @Override
                    public DialogInterface.OnClickListener setPositiveButtonClick() {
                        return (dialog, which) -> {
                            activity.recreate();
                        };
                    }

                    @Override
                    public String setNegativeButtonText() {
                        return "Cancel";
                    }

                    @Override
                    public DialogInterface.OnClickListener setNegativeButtonClick() {
                        return (dialog, which) -> {
                            if (finishActivity) {
                                activity.finish();
                            }
                        };
                    }

                    @Override
                    public String setNeutralButtonText() {
                        return null;
                    }

                    @Override
                    public DialogInterface.OnClickListener setNeutralButtonClick() {
                        return null;
                    }

                    @Override
                    public boolean setAutoShow() {
                        return true;
                    }

                    @Override
                    public DialogInterface.OnDismissListener setOnDismissListener() {
                        return null;
                    }
                };
            }
        }

        return connected;
    }

    default void checkContactExists(ReqCheckContactExists reqCheckContactExists) {

        startProgressDialog("", false, getCurrentActivity());

        WebServicesAPI webServicesAPI = RetrofitApiClient.getClient(getCurrentActivity()).create(WebServicesAPI.class);

        Call<ResCheckContactExists> call = webServicesAPI.userVerification(reqCheckContactExists);
        call.enqueue(new Callback<ResCheckContactExists>() {
            @Override
            public void onResponse(Call<ResCheckContactExists> call, Response<ResCheckContactExists> response) {

                try {

                    if (isErrorThrowing(response)) {
                        return;
                    }

                    if (isNeedRetroCallback) {
                        onRetroSuccess(api_checkContactExists, response);
                        return;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                    stopProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResCheckContactExists> call, Throwable t) {

                if (!isConnected(getCurrentActivity(), false, false)) {
                    stopProgressDialog();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.internetConnectionNotAvailMessage), true, getParentView());
                    return;
                }

                if (isNeedRetroCallback) {
                    onRetroFailure(api_checkContactExists, t);
                    return;
                }
                stopProgressDialog();
                try {
                    showSnakbar(t.getLocalizedMessage(), true, getParentView());
                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                }
            }
        });
    }

    default void oauthTokenLogin(ReqOauthTokenLogin reqOauthTokenLogin) {

        startProgressDialog("", false, getCurrentActivity());

        WebServicesAPI webServicesAPI = RetrofitApiClient.getClient(reqOauthTokenLogin.getUsername(), reqOauthTokenLogin.getPassword()).create(WebServicesAPI.class);

        Call<ResOauthTokenLogin> call = webServicesAPI.oauthTokenLogin(reqOauthTokenLogin.getGrant_type(), reqOauthTokenLogin.getUsername(), reqOauthTokenLogin.getPassword());
        call.enqueue(new Callback<ResOauthTokenLogin>() {
            @Override
            public void onResponse(Call<ResOauthTokenLogin> call, Response<ResOauthTokenLogin> response) {

                try {

                    if (isErrorThrowing(response)) {
                        removeGoogleAuth();
                        return;
                    }

                    if (isNeedRetroCallback) {
                        onRetroSuccess(api_oauthToken, response);
                        return;
                    }


                } catch (Exception e) {
                    removeGoogleAuth();
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                    stopProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResOauthTokenLogin> call, Throwable t) {
                removeGoogleAuth();
                if (!isConnected(getCurrentActivity(), false, false)) {
                    stopProgressDialog();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.internetConnectionNotAvailMessage), true, getParentView());
                    return;
                }

                if (isNeedRetroCallback) {
                    onRetroFailure(api_oauthToken, t);
                    return;
                }
                stopProgressDialog();
                try {
                    showSnakbar(t.getLocalizedMessage(), true, getParentView());
                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                }
            }
        });
    }

    default void userCreate(ReqUserCreate reqUserCreate) {

        startProgressDialog("", false, getCurrentActivity());

        WebServicesAPI webServicesAPI = RetrofitApiClient.getClient(getCurrentActivity()).create(WebServicesAPI.class);

        Call<ResUserCreate> call = webServicesAPI.userCreate(reqUserCreate);
        call.enqueue(new Callback<ResUserCreate>() {
            @Override
            public void onResponse(Call<ResUserCreate> call, Response<ResUserCreate> response) {

                try {

                    if (isErrorThrowing(response)) {
                        return;
                    }

                    if (isNeedRetroCallback) {
                        onRetroSuccess(api_userCreate, response);
                        return;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                    stopProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResUserCreate> call, Throwable t) {

                if (!isConnected(getCurrentActivity(), false, false)) {
                    stopProgressDialog();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.internetConnectionNotAvailMessage), true, getParentView());
                    return;
                }

                if (isNeedRetroCallback) {
                    onRetroFailure(api_userCreate, t);
                    return;
                }
                stopProgressDialog();
                try {
                    showSnakbar(t.getLocalizedMessage(), true, getParentView());
                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                }
            }
        });
    }

    default void forgotUpdatePassword(ReqForgotUpdatePassword reqUserCreate) {

        startProgressDialog("", false, getCurrentActivity());

        WebServicesAPI webServicesAPI = RetrofitApiClient.getClient(getCurrentActivity()).create(WebServicesAPI.class);

        Call<ResForgotUpdatePassword> call = webServicesAPI.forgotUpdatePassword(reqUserCreate);
        call.enqueue(new Callback<ResForgotUpdatePassword>() {
            @Override
            public void onResponse(Call<ResForgotUpdatePassword> call, Response<ResForgotUpdatePassword> response) {

                try {

                    if (isErrorThrowing(response)) {
                        return;
                    }

                    if (isNeedRetroCallback) {
                        onRetroSuccess(api_userUpdatePassword, response);
                        return;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                    stopProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResForgotUpdatePassword> call, Throwable t) {

                if (!isConnected(getCurrentActivity(), false, false)) {
                    stopProgressDialog();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.internetConnectionNotAvailMessage), true, getParentView());
                    return;
                }

                if (isNeedRetroCallback) {
                    onRetroFailure(api_userUpdatePassword, t);
                    return;
                }
                stopProgressDialog();
                try {
                    showSnakbar(t.getLocalizedMessage(), true, getParentView());
                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                }
            }
        });
    }

    default void getGoogleAuth(ReqGetGoogleAuth reqGetGoogleAuth) {

        startProgressDialog("", false, getCurrentActivity());

        WebServicesAPI webServicesAPI = RetrofitApiClient.getClient(getCurrentActivity()).create(WebServicesAPI.class);

        Call<ResGetGoogleAuth> call = webServicesAPI.getGoogleAuth(reqGetGoogleAuth.getEmailAddress());
        call.enqueue(new Callback<ResGetGoogleAuth>() {
            @Override
            public void onResponse(Call<ResGetGoogleAuth> call, Response<ResGetGoogleAuth> response) {

                try {

                    if (isErrorThrowing(response)) {
                        removeGoogleAuth();
                        return;
                    }

                    if (isNeedRetroCallback) {
                        onRetroSuccess(api_userGoogleSignUp, response);
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    removeGoogleAuth();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                    stopProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResGetGoogleAuth> call, Throwable t) {
                removeGoogleAuth();
                if (!isConnected(getCurrentActivity(), false, false)) {
                    stopProgressDialog();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.internetConnectionNotAvailMessage), true, getParentView());
                    return;
                }

                if (isNeedRetroCallback) {
                    onRetroFailure(api_userGoogleSignUp, t);
                    return;
                }
                stopProgressDialog();
                try {
                    showSnakbar(t.getLocalizedMessage(), true, getParentView());
                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                }
            }
        });
    }

    default void getUserDetails(ReqUserDetails reqUserDetails) {

        startProgressDialog("", false, getCurrentActivity());

        WebServicesAPI webServicesAPI = RetrofitApiClient.getClientViaAccessToken(getCurrentActivity()).create(WebServicesAPI.class);

        Call<ResUserDetails> call = webServicesAPI.getUserDetails(reqUserDetails.getUsername());
        call.enqueue(new Callback<ResUserDetails>() {
            @Override
            public void onResponse(Call<ResUserDetails> call, Response<ResUserDetails> response) {

                try {

                    if (isErrorThrowing(response)) {
                        removeGoogleAuth();
                        return;
                    }

                    if (isNeedRetroCallback) {
                        onRetroSuccess(api_profileUserDetail, response);
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                    stopProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResUserDetails> call, Throwable t) {
                removeGoogleAuth();
                if (!isConnected(getCurrentActivity(), false, false)) {
                    stopProgressDialog();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.internetConnectionNotAvailMessage), true, getParentView());
                    return;
                }

                if (isNeedRetroCallback) {
                    onRetroFailure(api_profileUserDetail, t);
                    return;
                }
                stopProgressDialog();
                try {
                    showSnakbar(t.getLocalizedMessage(), true, getParentView());
                } catch (Exception e) {
                    e.printStackTrace();
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                }
            }
        });
    }

    default boolean isErrorThrowing(Response response) {

        try {
            if (!isConnected(getCurrentActivity(), false, false)) {
                stopProgressDialog();
                showSnakbar(getCurrentActivity().getResources().getString(R.string.internetConnectionNotAvailMessage), true, getParentView());
                return true;
            }

            if (response == null) {
                stopProgressDialog();
                showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                return true;
            }


            if (response.body() == null) {
                stopProgressDialog();
                if (response.errorBody() != null) {
                    JSONObject jsonObject = new JSONObject(response.errorBody().string());
                    if (jsonObject.has(serialized_errorMessage_key)) {
                        showSnakbar(jsonObject.getString(serialized_errorMessage_key), true, getParentView());
                    } else {
                        showSnakbar(response.errorBody().string(), true, getParentView());
                    }
                } else {
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                }
                return true;
            }

            if (!response.isSuccessful()) {
                stopProgressDialog();
                if (response.errorBody() != null) {
                    showSnakbar(String.valueOf(response.errorBody().string()), true, getParentView());
                } else {
                    showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
                }
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
            return true;
        }
        return false;
    }


    default void removeGoogleAuth() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        GoogleSignIn.getClient(getCurrentActivity(), gso).signOut().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        });
    }
}
