package com.helpinghands.helperstations.hmutils.base;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.views.OKDialog;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerListener;

import java.util.ArrayList;

public class BaseLovFragment extends OKDialog implements OKRecyclerListener<Object> {

    public static final int LOV_ENTITY = 0;
    public static final int LOV_DEPARTMENT = 1;
    public static final int LOV_ACTIVITY = 2;
    public static final int LOV_SUBACTIVITY = 3;
    public static final int LOV_REQUESTNAME = 4;
    public static final int LOV_REASONNAME = 5;
    private Object objectArrayList;
    private TextView tvLovTitle;
    private int currentLovType;
    private OKRecyclerListener<Object> objectOKRecyclerListener;

    public BaseLovFragment(Context context, Fragment fragment, int currentLovType, Object objectArrayList) {
        super(context, false);
        this.objectArrayList = objectArrayList;
        this.currentLovType = currentLovType;
        setUI();
    }

    @Override
    protected int setDialogContentView() {
        return R.layout.fragment_base_lov;
    }

    @Override
    protected boolean setDialogCancelable() {
        return true;
    }

    @Override
    protected boolean setDialogCanceledOnTouchOutside() {
        return true;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initView(Dialog dialogView) {

        tvLovTitle = dialogView.findViewById(R.id.tvLovTitle);
        switch (currentLovType) {

        }

        initializeOkRecyclerView(true, dialogContext, getParentView(), false, 0);
        BaseLovAdapter baseLovAdapter = new BaseLovAdapter(BaseLovFragment.this, getCurrentContext());
        baseLovAdapter.setLovType(currentLovType);
        baseLovAdapter.addItems((ArrayList<Object>) objectArrayList);
        okRecyclerView.setAdapter(baseLovAdapter);
        baseLovAdapter.notifyDataSetChanged();
    }

    @Override
    protected DialogInterface.OnDismissListener onDialogDismissListner() {
        return null;
    }

    @Override
    protected boolean setAutoShow() {
        return true;
    }

    public OKRecyclerListener<Object> registerFabClick(OKRecyclerListener<Object> objectOKRecyclerListener) {
        this.objectOKRecyclerListener = objectOKRecyclerListener;
        return objectOKRecyclerListener;
    }

    @Override
    public void showEmptyDataView(int resId) {
        if (this.objectOKRecyclerListener != null) {
            this.objectOKRecyclerListener.showEmptyDataView(resId);
        }
    }

    @Override
    public void onRecyclerItemClick(int position, Object item) {
        if (this.objectOKRecyclerListener != null) {
            this.objectOKRecyclerListener.onRecyclerItemClick(position, item);
        }
        getDialog().dismiss();
    }

    @Override
    public void onEventFire(Class fromClass, String event, Object o) {

    }
}
