package com.helpinghands.helperstations.hmutils.base.listener;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerView;

public interface BaseMethodsInterface extends View.OnClickListener {

    int INTENT_RC_SIGN_IN = 101;
    int REQUEST_PERMISSION_LOCATION = 102;
    int REQUEST_PERMISSION_LOCATION_MUST = 103;
    int REQUEST_PERMISSION_WRITE_STORAGE = 104;
    int REQUEST_UPDATE_APP = 105;
    int INTENT_RQ_RESPONSE_OTP = 106;
    int REQUEST_USER_CONSENT = 107;

    void setOkRecyclerView(OKRecyclerView okRecyclerView, SwipeRefreshLayout swipeRefreshLayout);

    int setLayout();

    Activity getCurrentActivity();

    Context getCurrentContext();

    void initView();

    void startCode();

    int setMenu();

    @Override
    void onClick(View v);

    void onRetroSuccess(String method, Object object);

    void onRetroFailure(String method, Object object);

    View getParentView();

    String setToolbarTitle();

    void onSwipeRefresh();

    void setSwipeRefresh(boolean swipeRefresh);

    void onEventFire(Class fromClass, String event, Object o);

    void onLovItemSelect(int lovType, Object object);

}
