package com.helpinghands.helperstations.hmutils.base;

import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.listener.BaseInterFace;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

public abstract class BaseFragment extends BottomSheetDialogFragment implements BaseInterFace {

    public static boolean isNeedRetroCallback = false;
    public static boolean isBottomSheetOpened = false;
    public BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };
    protected String startDate = "", endDate = "";
    protected Context context;
    protected View view;
    protected OKRecyclerView okRecyclerView, okRecyclerView2;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected Toolbar toolbar;

    @Nullable
    protected Bundle savedInstanceState;
    private Calendar calendar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        this.savedInstanceState = savedInstanceState;
        view = inflater.inflate(setLayout(), container, false);
        context = getCurrentContext();
        initView();
        startCode();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((AppCompatActivity) getCurrentActivity()).getSupportActionBar() != null && !TextUtils.isEmpty(setToolbarTitle())) {
            Objects.requireNonNull(((AppCompatActivity) getCurrentActivity()).getSupportActionBar()).setTitle(setToolbarTitle());
        }
    }

    @Override
    public View getParentView() {
        return view.findViewById(R.id.parentView);
    }

    @Override
    public int setMenu() {
        return -1;
    }

    @Override
    public void onClick(View v) {

    }

    protected void initToolBar(boolean isContainMenu, boolean showBackOption) {

        try {
            toolbar = getParentView().findViewById(R.id.tool_bar);

            if (isContainMenu) {
                ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            }

            if (isContainMenu) {
                ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            }

            if (showBackOption) {

                ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOkRecyclerView(OKRecyclerView okRecyclerView, SwipeRefreshLayout swipeRefreshLayout) {
        if (okRecyclerView.getId() == R.id.okRecyclerView2) {
            this.okRecyclerView2 = okRecyclerView;
        } else {
            this.okRecyclerView = okRecyclerView;
        }
        if (swipeRefreshLayout != null) {
            this.swipeRefreshLayout = swipeRefreshLayout;
        }
    }

    protected String[] getCurrentWeek() {
        this.calendar = Calendar.getInstance();
        this.calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        this.calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return getNextWeek();
    }

    protected String[] getNextWeek() {
        DateFormat format = new SimpleDateFormat(yyyy_MM_dd_T_HH_mm_ss, Locale.US);
        String[] days = new String[7];
        for (int i = 0; i < 7; i++) {
            days[i] = format.format(this.calendar.getTime());
            this.calendar.add(Calendar.DATE, 1);
        }
        return days;
    }

    protected String[] getPreviousWeek() {
        this.calendar.add(Calendar.DATE, -14);
        return getNextWeek();
    }

    @Override
    public void onSwipeRefresh() {

    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && isVisible()) {
            if (((AppCompatActivity) getCurrentActivity()).getSupportActionBar() != null && !TextUtils.isEmpty(setToolbarTitle())) {
                Objects.requireNonNull(((AppCompatActivity) getCurrentActivity()).getSupportActionBar()).setTitle(setToolbarTitle());
            }
        }
    }

    @Override
    public void setSwipeRefresh(boolean swipeRefresh) {

        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(swipeRefresh);
        }
    }

    @Override
    public void onRetroSuccess(String method, Object object) {

        stopProgressDialog();
    }

    @Override
    public void onRetroFailure(String method, Object object) {

        stopProgressDialog();
    }

    @Override
    public void onEventFire(Class fromClass, String event, Object o) {

    }

    @Override
    public void onLovItemSelect(int lovType, Object object) {

    }

}
