package com.helpinghands.helperstations.hmutils.base;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerListener;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerViewAdapter;

import java.util.ArrayList;

public class BaseLovAdapter extends OKRecyclerViewAdapter<Object, OKRecyclerListener<Object>> {

    protected Context context;

    public BaseLovAdapter(OKRecyclerListener<Object> listener, Context context) {
        super(listener);
        this.context = context;
    }

    @Override
    public void onBindData(RecyclerView.ViewHolder holder, Object arrayListObject, int position) {

        BaseLovItemViewHolder myViewHolder = (BaseLovItemViewHolder) holder;
        switch (getLovType()) {

        }
    }

    @Override
    public RecyclerView.ViewHolder onBindViewHolder(ViewGroup parent, int viewType) {
        return new BaseLovItemViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_base_lov_fragment, parent, false));
    }

    @Override
    public ArrayList<String> compareFieldValue(Object item, ArrayList<String> searchItemList) {
        return null;
    }

    @Override
    public Activity getCurrentActivity() {
        return (Activity) listener;
    }

    @Override
    public Context getCurrentContext() {
        return context;
    }

    private static class BaseLovItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvValue;

        BaseLovItemViewHolder(View itemView) {
            super(itemView);
            tvValue = itemView.findViewById(R.id.tvValue);
        }
    }
}