package com.helpinghands.helperstations.hmutils.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class Preferences {

    private static Preferences preferences;
    private final String PREFS_NAME = "WFS_PREFS";

    public static Preferences getPreference() {
        if (preferences == null) {
            preferences = new Preferences();
        }
        return preferences;
    }

    public void clearAllPreferences(Context activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        editor.commit();
    }

    public void addString(Context context, int resKey, String value) {
        String key = context.getResources().getString(resKey);
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    public String getString(Context context, int resKey) {
        String key = context.getResources().getString(resKey);
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return TextUtils.isEmpty(sharedPreferences.getString(key, "")) ? "" : sharedPreferences.getString(key, "");
    }

    public void addBoolean(Context context, int resKey, boolean value) {
        String key = context.getResources().getString(resKey);
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
        editor.commit();
    }

    public boolean getBoolean(Context context, int resKey) {
        String key = context.getResources().getString(resKey);
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }


    public void addInteger(Context context, int resKey, int value) {
        String key = context.getResources().getString(resKey);
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
        editor.commit();
    }

    public int getInteger(Context context, int resKey) {
        String key = context.getResources().getString(resKey);
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, 0);
    }
}
