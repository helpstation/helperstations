package com.helpinghands.helperstations.hmutils.views;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;

import androidx.appcompat.app.AlertDialog;

public abstract class OKAlertDialog {

    private AlertDialog.Builder alertDialog;

    protected OKAlertDialog(Context context) {
        alertDialog = new AlertDialog.Builder(context);
        setUI();
    }

    private synchronized void setUI() {

        try {
            alertDialog.setCancelable(setCancelable());
            alertDialog.setTitle(setTitle());
            alertDialog.setMessage(setMessage());
            alertDialog.setIcon(setIcon());
            alertDialog.setPositiveButton(setPositiveButtonText(), setPositiveButtonClick());
            alertDialog.setNegativeButton(setNegativeButtonText(), setNegativeButtonClick());
            alertDialog.setNeutralButton(setNeutralButtonText(), setNeutralButtonClick());
            alertDialog.setOnDismissListener(setOnDismissListener());
            if (setAutoShow()) {
                alertDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public AlertDialog.Builder getDialog() {
        return alertDialog;
    }

    public abstract boolean setCancelable();

    public abstract String setTitle();

    public abstract String setMessage();

    public abstract Drawable setIcon();

    public abstract String setPositiveButtonText();

    public abstract DialogInterface.OnClickListener setPositiveButtonClick();

    public abstract String setNegativeButtonText();

    public abstract DialogInterface.OnClickListener setNegativeButtonClick();

    public abstract String setNeutralButtonText();

    public abstract DialogInterface.OnClickListener setNeutralButtonClick();

    public abstract boolean setAutoShow();

    public abstract DialogInterface.OnDismissListener setOnDismissListener();

    public AlertDialog.Builder getAlertDialog() {
        return alertDialog;
    }
}
