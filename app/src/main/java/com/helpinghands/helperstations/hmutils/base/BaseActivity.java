package com.helpinghands.helperstations.hmutils.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.SplashActivity;
import com.helpinghands.helperstations.dashboard.fragment.homefragment.HomeFragment;
import com.helpinghands.helperstations.hmutils.base.listener.BaseInterFace;
import com.helpinghands.helperstations.hmutils.preferences.LocationData;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;
import com.helpinghands.helperstations.hmutils.views.OKAlertDialog;
import com.helpinghands.helperstations.hmutils.views.OKDialog;
import com.helpinghands.helperstations.hmutils.views.OKProgressDialog;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public abstract class BaseActivity extends AppCompatActivity implements BaseInterFace {

    public static Snackbar snackbar;
    public static OKProgressDialog okProgressDialog = null;
    @SuppressLint("StaticFieldLeak")
    public static TextView tvNoDataFound;
    @SuppressLint("StaticFieldLeak")
    protected String TAG = getCurrentActivity().getClass().getSimpleName();
    protected String newFirebaseToken = "";
    protected Activity activity;
    protected Context context;
    protected FragmentManager fragmentManager;
    protected FragmentTransaction fragmentTransaction;
    protected List<Fragment> fragmentsList = new ArrayList<>();
    protected List<WeakReference<Fragment>> weakReferenceFragmentList = new ArrayList<WeakReference<Fragment>>();
    protected OKAlertDialog okAlertDialog = null;
    protected OKDialog okDialog = null;
    protected OKRecyclerView okRecyclerView, okRecyclerView2;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected Toolbar toolbar;
    protected TextView tvToolBarTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        activity = getCurrentActivity();
        context = getCurrentContext();
        setContentView(setLayout());
        checkAppUpdateAvailable();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (((AppCompatActivity) getCurrentActivity()).getSupportActionBar() != null && !TextUtils.isEmpty(setToolbarTitle())) {
                Objects.requireNonNull(((AppCompatActivity) getCurrentActivity()).getSupportActionBar()).setTitle(setToolbarTitle());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View getParentView() {
        return findViewById(R.id.parentView);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public int setMenu() {
        return -1;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (setMenu() != -1) {
            getMenuInflater().inflate(setMenu(), menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void initToolBar(boolean isContainMenu, boolean showBackOption) {

        try {
            toolbar = findViewById(R.id.tool_bar);

            if (isContainMenu) {
                setSupportActionBar(toolbar);
            }

            if (showBackOption) {

                if (toolbar != null)
                    setSupportActionBar(toolbar);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setTitle(setToolbarTitle());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setFragment(Fragment fragment) {

        try {
            String backStateName = fragment.getClass().getName();

            boolean isCalled = false;
            fragmentsList = new ArrayList<>();
            fragmentManager = getSupportFragmentManager();
            fragmentsList = getActiveFragments();

            for (Fragment fragments : fragmentsList) {

                if (fragments.getClass().getName().equalsIgnoreCase(backStateName))
                    isCalled = true;
            }

            if (!isCalled) {

                if (fragment instanceof HomeFragment) {
                    clearBackStack();
                }

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentContainer, fragment);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fragmentTransaction.addToBackStack(backStateName);
                weakReferenceFragmentList.add(new WeakReference(fragment));
                fragmentTransaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* ***  Clear all stack if user visit Dashboard Fragment again *** */
    protected void clearBackStack() {

        fragmentManager = getSupportFragmentManager();

        if (fragmentManager.getBackStackEntryCount() > 0) {

            FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(0);
            fragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    /*
     *   This function will return list of active fragments
     *   you can identify which fragments are added in backstack so It would help you to prevent adding any currentFragment more than one time
     *
     * */
    protected List<Fragment> getActiveFragments() {

        ArrayList<Fragment> returnFragments = new ArrayList<Fragment>();

        for (WeakReference<Fragment> weakReference : weakReferenceFragmentList) {

            Fragment weakFragments = weakReference.get();

            if (weakFragments != null) {

                if (weakFragments.isVisible())
                    returnFragments.add(weakFragments);
            }
        }

        return returnFragments;
    }

    protected void replaceFragments(Class fragmentClass) {

        Fragment fragment = null;

        try {

            fragment = (Fragment) fragmentClass.newInstance();
            // Insert the currentFragment by replacing any existing currentFragment
            fragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOkRecyclerView(OKRecyclerView okRecyclerView, SwipeRefreshLayout swipeRefreshLayout) {
        if (okRecyclerView.getId() == R.id.okRecyclerView2) {
            this.okRecyclerView2 = okRecyclerView;
        } else {
            this.okRecyclerView = okRecyclerView;
        }
        if (swipeRefreshLayout != null) {
            this.swipeRefreshLayout = swipeRefreshLayout;
        }
    }

    protected void setupBadge(TextView textCartItemCount, int mCartItemCount) {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 999)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public boolean setCurrentLocation(int code) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            }
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2001);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                }
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, code);
            } else {
                getCurrentLocationAndAddress(LocationData.getInstance().currentLocation);
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocationAndAddress(LocationData.getInstance().currentLocation);
                } else {
                    setCurrentLocation(REQUEST_PERMISSION_LOCATION);
                }
                break;

            case REQUEST_PERMISSION_WRITE_STORAGE:
                break;

            case REQUEST_PERMISSION_LOCATION_MUST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocationAndAddress(LocationData.getInstance().currentLocation);
                } else {
                    setCurrentLocation(REQUEST_PERMISSION_LOCATION);
                }
                break;
        }
    }

    @Override
    public void onSwipeRefresh() {

    }

    @Override
    public void onRetroSuccess(String method, Object object) {
        stopProgressDialog();
    }

    @Override
    public void onRetroFailure(String method, Object object) {
        stopProgressDialog();
        if (object instanceof Throwable) {
            Throwable t = (Throwable) object;
            try {
                showSnakbar(t.getLocalizedMessage(), true, getParentView());
            } catch (Exception e) {
                e.printStackTrace();
                showSnakbar(getCurrentActivity().getResources().getString(R.string.apiErrorMessage), true, getParentView());
            }
        }
    }


    protected void checkAppUpdateAvailable() {

        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(getCurrentActivity());
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {

                // Request the update.
                try {
                    appUpdateManager.startUpdateFlowForResult(
                            appUpdateInfo,
                            AppUpdateType.IMMEDIATE,
                            getCurrentActivity(),
                            REQUEST_UPDATE_APP);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                    initView();
                    startCode();
                    setCurrentLocation(REQUEST_PERMISSION_LOCATION);
                }
            } else {
                initView();
                startCode();
                setCurrentLocation(REQUEST_PERMISSION_LOCATION);
            }
        });
        appUpdateInfoTask.addOnFailureListener(e -> {
            e.printStackTrace();
            initView();
            startCode();
            setCurrentLocation(REQUEST_PERMISSION_LOCATION);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_UPDATE_APP) {
            if (resultCode != RESULT_OK) {
                checkAppUpdateAvailable();
            }
        }
    }

    @Override
    public void setSwipeRefresh(boolean swipeRefresh) {

        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(swipeRefresh);
        }
    }

    @Override
    public void onEventFire(Class fromClass, String event, Object o) {

    }

    @Override
    public void onLovItemSelect(int lovType, Object object) {

    }

    protected void makeLogout() {

        if (Preferences.getPreference().getBoolean(getCurrentContext(), R.string.pref_userLoggedIn_via_google)) {

            startProgressDialog("", false, getCurrentActivity());

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            GoogleSignIn.getClient(this, gso).signOut().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                    Preferences.getPreference().clearAllPreferences(getCurrentContext());
                    stopProgressDialog();
                    Intent intent = new Intent(getCurrentContext(), SplashActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            });
            return;
        }

        Preferences.getPreference().clearAllPreferences(getCurrentContext());
        stopProgressDialog();
        Intent intent = new Intent(getCurrentContext(), SplashActivity.class);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
