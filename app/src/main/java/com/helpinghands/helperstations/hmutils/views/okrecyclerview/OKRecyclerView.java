package com.helpinghands.helperstations.hmutils.views.okrecyclerview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.helpinghands.helperstations.R;

public class OKRecyclerView extends RecyclerView {

    private TextView tvEmptyMsgHolder;
    private Context context;

    public OKRecyclerView(Context context) {
        super(context);
        this.context = context;
    }

    public OKRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public OKRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setVerticalScrollBarEnabled(boolean verticalScrollBarEnabled) {
        super.setVerticalScrollBarEnabled(true);
    }

    @Override
    public void setVerticalScrollbarThumbDrawable(@Nullable Drawable drawable) {
        super.setVerticalScrollbarThumbDrawable(context.getResources().getDrawable(R.drawable.thumb_drawable));
    }

    @Override
    public void setVerticalScrollbarTrackDrawable(@Nullable Drawable drawable) {
        super.setVerticalScrollbarTrackDrawable(context.getResources().getDrawable(R.drawable.line_drawable));
    }

    public View getEmptyMsgHolder() {
        return tvEmptyMsgHolder;
    }

    public void setEmptyMsgHolder(TextView tvEmptyMsgHolder) {
        this.tvEmptyMsgHolder = tvEmptyMsgHolder;
    }

    public void showEmptyDataView(String errorMessage) {
        if (tvEmptyMsgHolder != null) {
            if (getAdapter() != null && getAdapter().getItemCount() == 0) {

                tvEmptyMsgHolder.setVisibility(View.VISIBLE);
                tvEmptyMsgHolder.setText(errorMessage);

            } else {
                tvEmptyMsgHolder.setVisibility(View.GONE);
            }
        }
    }
}
