package com.helpinghands.helperstations.hmutils.views;

import android.app.Dialog;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.helpinghands.helperstations.R;

public class OKProgressDialog extends Dialog {

    public static OKProgressDialog show(Context context, CharSequence title, CharSequence message) {
        return show(context, title, message, false);
    }

    private static OKProgressDialog show(Context context, CharSequence title, CharSequence message, boolean indeterminate) {
        return show(context, title, message, indeterminate, false, null);
    }

    public static OKProgressDialog show(Context context, CharSequence title, CharSequence message, boolean indeterminate, boolean cancelable) {
        return show(context, title, message, indeterminate, cancelable, null);
    }

    private static OKProgressDialog show(Context context, CharSequence title, CharSequence message, boolean indeterminate, boolean cancelable, OnCancelListener cancelListener) {

        OKProgressDialog dialog = null;

        try {
            dialog = new OKProgressDialog(context);
            dialog.setTitle(title);
            dialog.setCancelable(cancelable);
            dialog.setOnCancelListener(cancelListener);

            /* The next line will add the ProgressBar to the dialog. */
            dialog.addContentView(new ProgressBar(context), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dialog;
    }

    private OKProgressDialog(Context context) {
        super(context, R.style.OKProgressDialogTheme);
    }
}