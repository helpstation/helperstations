package com.helpinghands.helperstations.hmutils.room;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

//@Database(entities = {
//
//}, version = 1, exportSchema = false)
public abstract class HMRoomDB extends RoomDatabase {

    public static final String HMROOMDBNAME = "HM_ROOM_DB";
    public static HMRoomDB getHMRoomDB;

    public static HMRoomDB getWFSRoomDB(Context context) {
        if (getHMRoomDB == null) {
            getHMRoomDB = Room.databaseBuilder(context.getApplicationContext(), HMRoomDB.class, HMROOMDBNAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return getHMRoomDB;
    }

}
