package com.helpinghands.helperstations.hmutils.views.okrecyclerview;

import androidx.annotation.StringRes;

public interface OKRecyclerListener<T> {

    void showEmptyDataView(@StringRes int resId);

    void onRecyclerItemClick(int position, T item);

    default void onEventFire(Object[] objects) {

    }
}
