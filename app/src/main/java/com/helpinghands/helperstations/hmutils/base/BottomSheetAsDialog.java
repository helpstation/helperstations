package com.helpinghands.helperstations.hmutils.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.listener.BaseMethodsInterface;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerListener;

import java.util.ArrayList;

public class BottomSheetAsDialog extends BaseFragment implements View.OnClickListener, OKRecyclerListener<Object>, SearchView.OnQueryTextListener {

    protected Gson gson = null;
    private Context context;
    private int lovType;
    private SearchView searchView = null;
    private int LOV_CURRENT;
    private BaseLovAdapter baseLovAdapter;
    private Object arrayListObject;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    public BottomSheetAsDialog(int lovType, Object arrayListObject, Context context) {
        this.LOV_CURRENT = lovType;
        this.lovType = lovType;
        this.arrayListObject = arrayListObject;
        this.context = context;
        gson = new Gson();
        isBottomSheetOpened = true;
    }

    @Override
    public int setLayout() {
        return R.layout.bottomsheet_as_dialog_generic;
    }

    @Override
    public Activity getCurrentActivity() {
        return getActivity();
    }

    @Override
    public Context getCurrentContext() {
        return context;
    }

    @Override
    public void initView() {
        initToolBar(true, false);
        initializeOkRecyclerView(true, BottomSheetAsDialog.this.getContext(), view, false, 0);
        searchView = view.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);
        baseLovAdapter = new BaseLovAdapter(BottomSheetAsDialog.this, getActivity());
        baseLovAdapter.addItems((ArrayList<Object>) arrayListObject);
        baseLovAdapter.setLovType(lovType);
        okRecyclerView.setAdapter(baseLovAdapter);
        baseLovAdapter.notifyDataSetChanged();
    }

    @Override
    public void startCode() {

    }

    @Override
    public String setToolbarTitle() {
        switch (lovType) {
        }
        return "";
    }

    @Override
    public void showEmptyDataView(int resId) {

    }

    @Override
    public void onRecyclerItemClick(int position, Object item) {
        BaseMethodsInterface clickListener = (BaseMethodsInterface) context;
        if (clickListener != null) {
            try {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            clickListener.onLovItemSelect(LOV_CURRENT, item);
        }
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        baseLovAdapter.getFilter().filter(newText);
        return false;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        isBottomSheetOpened = false;
    }
}
