package com.helpinghands.helperstations.hmutils.retrofit;

import com.helpinghands.helperstations.authorization.login.retrofit.ResGetGoogleAuth;
import com.helpinghands.helperstations.authorization.login.retrofit.ResOauthTokenLogin;
import com.helpinghands.helperstations.authorization.login.retrofit.ResUserDetails;
import com.helpinghands.helperstations.authorization.resetuserauth.retrofit.ReqForgotUpdatePassword;
import com.helpinghands.helperstations.authorization.resetuserauth.retrofit.ResForgotUpdatePassword;
import com.helpinghands.helperstations.authorization.signup.retrofit.ReqCheckContactExists;
import com.helpinghands.helperstations.authorization.signup.retrofit.ReqUserCreate;
import com.helpinghands.helperstations.authorization.signup.retrofit.ResCheckContactExists;
import com.helpinghands.helperstations.authorization.signup.retrofit.ResUserCreate;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface WebServicesAPI {

    String api_oauthToken = "oauth/token";
    String api_checkContactExists = "user/verification";
    String api_userCreate = "user/create";
    String api_userUpdatePassword = "user/updatePassword";
    String api_userGoogleSignUp = "user/googleSignUp";
    String api_profileUserDetail = "profile/userDetail";

    @POST(api_oauthToken)
    Call<ResOauthTokenLogin> oauthTokenLogin(
            @Query("grant_type") String grant_type,
            @Query("username") String username,
            @Query("password") String password);

    @POST(api_checkContactExists)
    Call<ResCheckContactExists> userVerification(@Body ReqCheckContactExists reqCheckContactExists);

    @POST(api_userCreate)
    Call<ResUserCreate> userCreate(@Body ReqUserCreate reqUserCreate);

    @PUT(api_userUpdatePassword)
    Call<ResForgotUpdatePassword> forgotUpdatePassword(@Body ReqForgotUpdatePassword reqUserCreate);

    @GET(api_userGoogleSignUp)
    Call<ResGetGoogleAuth> getGoogleAuth(@Query("email") String email);

    @GET(api_profileUserDetail)
    Call<ResUserDetails> getUserDetails(@Query("username") String username);

}