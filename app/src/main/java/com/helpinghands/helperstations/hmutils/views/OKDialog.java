package com.helpinghands.helperstations.hmutils.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.base.listener.BaseInterFace;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerView;

public abstract class OKDialog implements BaseInterFace {

    protected Dialog genericDialog;
    protected Context dialogContext;
    protected OKRecyclerView okRecyclerView;

    protected OKDialog(Context context, boolean needSetup) {
        this.dialogContext = context;
        genericDialog = new Dialog(context);
        if (needSetup) {
            setUI();
        }
    }

    protected synchronized void setUI() {

        try {
            genericDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            genericDialog.setContentView(setDialogContentView());
            initView(getDialog());
            genericDialog.setCancelable(setDialogCancelable());
            genericDialog.setCanceledOnTouchOutside(setDialogCanceledOnTouchOutside());
            genericDialog.setOnDismissListener(onDialogDismissListner());
            if (setAutoShow()) {
                try {
                    Window window = genericDialog.getWindow();
                    if (window != null) {
                        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                genericDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected abstract int setDialogContentView();

    protected abstract boolean setDialogCancelable();

    protected abstract boolean setDialogCanceledOnTouchOutside();

    protected abstract void initView(Dialog dialogView);

    protected abstract DialogInterface.OnDismissListener onDialogDismissListner();

    protected abstract boolean setAutoShow();

    public Dialog getDialog() {
        return genericDialog;
    }

    /* ***************** UNUSED IMPORTS **************** */

    @Override
    public int setLayout() {
        return 0;
    }

    @Override
    public Activity getCurrentActivity() {
        return null;
    }

    @Override
    public Context getCurrentContext() {
        return null;
    }

    @Override
    public void initView() {

    }

    @Override
    public void startCode() {

    }

    @Override
    public int setMenu() {
        return 0;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onRetroSuccess(String method, Object object) {

    }

    @Override
    public void onRetroFailure(String method, Object object) {

    }

    @Override
    public View getParentView() {
        return genericDialog.findViewById(R.id.parentView);
    }

    @Override
    public void setOkRecyclerView(OKRecyclerView okRecyclerView, SwipeRefreshLayout swipeRefreshLayout) {
        this.okRecyclerView = okRecyclerView;
    }

    @Override
    public String setToolbarTitle() {
        return "";
    }

    @Override
    public void onSwipeRefresh() {

    }

    @Override
    public void setSwipeRefresh(boolean swipeRefresh) {

    }

    @Override
    public void onLovItemSelect(int lovType, Object object) {

    }

}
