package com.helpinghands.helperstations.hmutils.base.listener;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.helpinghands.helperstations.hmutils.preferences.LocationData;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Locale;

public interface BaseInterFace extends BaseApiCallInterface, LocationListener {

    @Override
    default void onLocationChanged(Location location) {
        LocationData.getInstance().currentlatitude = location.getLatitude();
        LocationData.getInstance().currentlongitude = location.getLongitude();

        if (LocationData.getInstance().googleMap != null) {
            LatLng currentLatLong = new LatLng(LocationData.getInstance().currentlatitude, LocationData.getInstance().currentlongitude);
            LocationData.getInstance().googleMap.addMarker(new MarkerOptions().position(currentLatLong).title("You are here."));
            LocationData.getInstance().googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLong));
            LocationData.getInstance().googleMap.setMaxZoomPreference(LocationData.getInstance().googleMap.getMaxZoomLevel());
        }
    }

    @Override
    default void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    default void onProviderEnabled(String provider) {
    }

    @Override
    default void onProviderDisabled(String provider) {
    }

    default boolean turnGPSOn() {
        String provider = Settings.Secure.getString(getCurrentActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            getCurrentActivity().startActivity(intent);
            return false;
        }
        return true;
    }

    //method to convert the selected image to base64 encoded string
    default String convertBitmapToString(Bitmap bitmap) {
        String encodedImage = "", base64StringImage = "";
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        try {
//            encodedImage = URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
//            Log.d("TAG", "IMAGE:: "+encodedImage);
            base64StringImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            Log.d("TAG", "IMAGE w/o URLend:: " + base64StringImage);
        } catch (AssertionError e) {
            e.printStackTrace();
        }

        return base64StringImage;
    }

    default String getCurrentLocationAndAddress(Location location) {

        LocationManager locationManager = (LocationManager) getCurrentActivity().getSystemService(Context.LOCATION_SERVICE);

        if (locationManager != null) {

            try {

                if (Build.VERSION.SDK_INT >= 23
                        &&
                        getCurrentActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        &&
                        getCurrentActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return "";
                }
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        1000,
                        10, BaseInterFace.this);
            } catch (Exception ex) {
                Log.i("msg", "fail to request location update, ignore", ex);
            }
            Criteria criteria = new Criteria();

            List<String> providers = locationManager.getProviders(criteria, true);
            for (String provider : providers) {
                Location l = locationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (location == null || l.getAccuracy() < location.getAccuracy()) {
                    // Found best last known location: %s", l);
                    location = l;
                }
            }

            if (location == null) {
                return "";
            }

            Geocoder gcd = new Geocoder(getCurrentActivity(), Locale.getDefault());
            List<Address> addresses;
            try {
                LocationData.getInstance().currentlatitude = location.getLatitude();
                LocationData.getInstance().currentlongitude = location.getLongitude();
                addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {


                    String address = ""; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    try {
                        address = addresses.get(0).getAddressLine(0);
                    } catch (Exception e) {
                        address = "";
                        e.printStackTrace();
                    }


                    String locality = "";
                    try {
                        locality = addresses.get(0).getLocality();
                    } catch (Exception e) {
                        locality = "";
                        e.printStackTrace();
                    }

                    String subLocality = "";
                    try {
                        subLocality = addresses.get(0).getSubLocality();
                    } catch (Exception e) {
                        subLocality = "";
                        e.printStackTrace();
                    }


                    String state = "";
                    try {
                        state = addresses.get(0).getAdminArea();
                    } catch (Exception e) {
                        state = "";
                        e.printStackTrace();
                    }


                    String country = "";
                    try {
                        country = addresses.get(0).getCountryName();
                    } catch (Exception e) {
                        country = "";
                        e.printStackTrace();
                    }


                    String postalCode = "";
                    try {
                        postalCode = addresses.get(0).getPostalCode();
                    } catch (Exception e) {
                        postalCode = "";
                        e.printStackTrace();
                    }


                    String knownName = "";
                    try {
                        knownName = addresses.get(0).getFeatureName();
                    } catch (Exception e) {
                        knownName = "";
                        e.printStackTrace();
                    }

                    LocationData.getInstance().currentLocationAddress = (TextUtils.isEmpty(address) ? "" : address.concat(" , "))
                            .concat(TextUtils.isEmpty(locality) ? "" : locality.concat(" , "))
                            .concat(TextUtils.isEmpty(subLocality) ? "" : subLocality.concat(" , "))
                            .concat(TextUtils.isEmpty(state) ? "" : state.concat(" , "))
                            .concat(TextUtils.isEmpty(country) ? "" : country.concat(" , "))
                            .concat(TextUtils.isEmpty(postalCode) ? "" : postalCode);
                }

            } catch (Exception e) {
                e.printStackTrace();
                LocationData.getInstance().currentLocationAddress = "";
            }
        }
        return LocationData.getInstance().currentLocationAddress;
    }
}
