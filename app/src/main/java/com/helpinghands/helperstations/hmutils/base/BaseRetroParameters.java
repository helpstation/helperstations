package com.helpinghands.helperstations.hmutils.base;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;

public class BaseRetroParameters {

    @SerializedName("username")
    @Expose
    private String username = "";

    public BaseRetroParameters(Context context) {
        setUsername(Preferences.getPreference().getString(context, R.string.pref_api_param_username));
    }

    public BaseRetroParameters() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
