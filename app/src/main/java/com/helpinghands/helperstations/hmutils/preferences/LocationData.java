package com.helpinghands.helperstations.hmutils.preferences;

import android.location.Location;

import com.google.android.gms.maps.GoogleMap;

public class LocationData {

    public Location currentLocation;
    public Double currentlatitude = 0.0;
    public Double currentlongitude = 0.0;
    public String currentLocationAddress = "";
    public GoogleMap googleMap;

    public static LocationData locationData;
    public static LocationData getInstance() {
        if (locationData == null) {
            locationData = new LocationData();
        }
        return locationData;
    }
}
