package com.helpinghands.helperstations.hmutils.retrofit;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.helpinghands.helperstations.hmutils.utils.AESEncrypt.encrypt;

public class RetrofitApiClient {

    //    http://ec2-3-17-13-230.us-east-2.compute.amazonaws.com:1800/helperstations/swagger-ui.html#!/User_API's // swager
//    public static final String BASEURL = "http://3.17.13.230:1800/helperstations/";  // AWS Live
//    public static final String BASEURL = "http://ec2-18-217-151-18.us-east-2.compute.amazonaws.com:1800/helperstations/";
    public static final String BASEURL = "http://1dfedce87910.ngrok.io/helperstations/";

    private static String basicAuth = "";
    private static String accessToken = "";

    public static Retrofit getClient(String userName, String password) {

        userName = "clientId";
        password = "secret";

        String credentials = "";
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(password)) {
            credentials = userName + ":" + password;
            basicAuth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        } else {
            basicAuth = "";
        }
        return createRetrofitClient();
    }

    public static Retrofit getClient(Context context) {
        return createRetrofitClient();
    }

    public static Retrofit getClientViaAccessToken(Context context) {
        basicAuth = "";
        accessToken = "bearer " + Preferences.getPreference().getString(context, R.string.pref_api_param_auth_token);
        return createRetrofitClient();
    }

    public static Retrofit createRetrofitClient() {

        Retrofit retrofit = null;

        try {

            OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
            okHttpClientBuilder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    Log.d("Verfiying Hostname ****", hostname);
                    return true;
                }
            });

            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            okHttpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder requestBuilder;

                    if (!TextUtils.isEmpty(basicAuth)) {
                        requestBuilder = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Accept", "application/json")
                                .header("Authorization", basicAuth)
                                .method(original.method(), original.body());

                    } else if (!TextUtils.isEmpty(accessToken)) {
                        requestBuilder = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Accept", "application/json")
                                .header("Authorization", accessToken)
                                .method(original.method(), original.body());

                    } else {
                        requestBuilder = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Accept", "application/json")
                                .method(original.method(), original.body());
                    }
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            okHttpClientBuilder.build();

            okHttpClientBuilder.connectTimeout(2,
                    TimeUnit.MINUTES);
            okHttpClientBuilder.writeTimeout(2,
                    TimeUnit.MINUTES);
            okHttpClientBuilder.readTimeout(2,
                    TimeUnit.MINUTES);

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClientBuilder.interceptors().add(interceptor);

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASEURL)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setLenient().create()))
                    .client(okHttpClientBuilder.build()).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retrofit;
    }


    private static class EncryptionInterceptor implements Interceptor {

        private static final boolean DEBUG = true;
        private final String TAG = EncryptionInterceptor.class.getSimpleName();

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            RequestBody oldBody = request.body();
            Buffer buffer = new Buffer();
            if (oldBody != null) {
                oldBody.writeTo(buffer);
            }
            String strOldBody = buffer.readUtf8();

            MediaType mediaType = MediaType.parse("text/plain; charset=utf-8");
            String strNewBody = null;
            try {
                strNewBody = encrypt(strOldBody);
                RequestBody body = RequestBody.create(mediaType, strNewBody);
                request = request.newBuilder().header("Content-Type", body.contentType().toString()).header("Content-Length", String.valueOf(body.contentLength())).method(request.method(), body).build();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return chain.proceed(request);
        }
    }
}