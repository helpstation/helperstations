package com.helpinghands.helperstations.hmutils.base.listener;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.views.OKAlertDialog;
import com.helpinghands.helperstations.hmutils.views.OKProgressDialog;
import com.helpinghands.helperstations.hmutils.views.okrecyclerview.OKRecyclerView;

import java.util.Objects;

import static android.content.Context.KEYGUARD_SERVICE;
import static com.helpinghands.helperstations.hmutils.base.BaseActivity.okProgressDialog;
import static com.helpinghands.helperstations.hmutils.base.BaseActivity.snackbar;
import static com.helpinghands.helperstations.hmutils.base.BaseActivity.tvNoDataFound;

public interface BaseViewInterface extends BaseMethodsInterface {

    default void initializeOkRecyclerView(boolean showLineDecorView, Context context, View view, boolean isContainSwipeRefreshLayout, int recyclerViewOtherId) {
        try {
            OKRecyclerView okRecyclerView;
            if (recyclerViewOtherId != 0) {
                okRecyclerView = view.findViewById(recyclerViewOtherId);
            } else {
                okRecyclerView = view.findViewById(R.id.okRecyclerView);
            }

            setOkRecyclerView(okRecyclerView, null);
            tvNoDataFound = view.findViewById(R.id.tvNoDataFound);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            okRecyclerView.setLayoutManager(linearLayoutManager);
            if (showLineDecorView) {
                okRecyclerView.setItemAnimator(new DefaultItemAnimator());
                okRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
            }
            okRecyclerView.setEmptyMsgHolder(tvNoDataFound);

            if (isContainSwipeRefreshLayout) {
                SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            onSwipeRefresh();
                        }
                    });
                    setOkRecyclerView(okRecyclerView, swipeRefreshLayout);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default void collapseViewVertical(final View v, int duration) {

        try {

            final int initialHeight = v.getMeasuredHeight();

            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    if (interpolatedTime == 1) {
                        v.setVisibility(View.GONE);
                    } else {
                        v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                        v.requestLayout();
                    }
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };

            if (duration != -1) {
                a.setDuration(duration);
            } else {
                // 1dp/ms
                a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
            }
            v.startAnimation(a);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default void showSnakbar(String msg, boolean showIndifinite, View view) {

        try {

            if (showIndifinite) {

                snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });

                snackbar.setActionTextColor(view.getContext().getResources().getColor(R.color.white));
                View snackbarView = snackbar.getView();

                try {
                    snackbarView.setBackgroundColor(view.getContext().getResources().getColor(R.color.colorPrimaryDark));
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }

                TextView textView = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setMaxLines(20);
                snackbar.show();
            } else {
                snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
                View snackbarView = snackbar.getView();

                try {
                    snackbarView.setBackgroundColor(view.getContext().getResources().getColor(R.color.colorPrimaryDark));
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }

                TextView textView = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setMaxLines(20);
                snackbar.show();
            }
            hideSoftKeyboard(view.getContext(), view);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                Toast.makeText(view.getContext(), msg, showIndifinite ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    @SuppressLint("HardwareIds")
    default String getDeviceIMEI(Activity activity) {

        String deviceUniqueIdentifier = null;

        TelephonyManager tm = (TelephonyManager) Objects.requireNonNull(activity).getSystemService(Context.TELEPHONY_SERVICE);

        if (null != tm) {

            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                deviceUniqueIdentifier = tm.getSubscriberId();
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
            }
        }

        if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
            deviceUniqueIdentifier = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        }

        return deviceUniqueIdentifier;
    }

    default void expandViewVertical(final View v, int duration) {

        try {

            v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final int targetHeight = v.getMeasuredHeight();

            // Older versions of android (pre API 21) cancel animations for views with a height of 0.
            v.getLayoutParams().height = 1;
            v.setVisibility(View.VISIBLE);
            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    v.getLayoutParams().height = interpolatedTime == 1 ? ViewGroup.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime);
                    v.requestLayout();
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };

            if (duration != -1) {
                a.setDuration(duration);
            } else {
                a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
            }
            // 1dp/ms
            v.startAnimation(a);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default void expandViewHorizontal(final View v) {

        try {

            v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final int targetHeight = v.getMeasuredWidth();

            // Older versions of android (pre API 21) cancel animations for views with a height of 0.
            v.getLayoutParams().width = 1;
            v.setVisibility(View.VISIBLE);
            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    v.getLayoutParams().width = interpolatedTime == 1 ? ViewGroup.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime);
                    v.requestLayout();
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };

            // 1dp/ms
            a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
            v.startAnimation(a);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default void collapseViewHorizontal(final View v) {

        try {

            final int initialHeight = v.getMeasuredWidth();

            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    if (interpolatedTime == 1) {
                        v.setVisibility(View.GONE);
                    } else {
                        v.getLayoutParams().width = initialHeight - (int) (initialHeight * interpolatedTime);
                        v.requestLayout();
                    }
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };

            // 1dp/ms
            a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
            v.startAnimation(a);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default void hideSoftKeyboard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* **************       Method for visible component **************** */
    default void visibleView(final View view) {
        try {
            if (view != null) {
                if (view.getVisibility() == View.GONE) {
                    view.setVisibility(View.VISIBLE);
                    view.animate().alpha(1f).setDuration(500).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            view.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* ****************       Method for Hide component ***************** */
    default void goneView(final View view) {
        try {
            if (view != null) {
                if (view.getVisibility() == View.VISIBLE) {
                    view.animate().alpha(0.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            view.setVisibility(View.GONE);
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* ****************      Enable Click event for any component *********************** */
    default void enableView(View view) {
        try {
            if (view != null) {
                if (!view.isEnabled()) {
                    view.setEnabled(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* ******************      Desable Click event for any component ********************** */
    default void disableView(View view) {
        try {
            if (view != null) {
                if (view.isEnabled()) {
                    view.setEnabled(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default void enableDisableView(View view, boolean enabled) {
        try {
            if (view != null) {
                view.setEnabled(enabled);
                if (view instanceof ViewGroup) {
                    ViewGroup group = (ViewGroup) view;
                    for (int idx = 0; idx < group.getChildCount(); idx++) {
                        enableDisableView(group.getChildAt(idx), enabled);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default void startProgressDialog(String title, boolean cancelable, Activity activity) {
        try {
            if (okProgressDialog == null) {
                okProgressDialog = OKProgressDialog.show(activity, title, "", true, cancelable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default void stopProgressDialog() {
        try {

            setSwipeRefresh(false);

            if (okProgressDialog != null) {
                okProgressDialog.dismiss();
                okProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default Boolean checkBiometricSupport() {

        KeyguardManager keyguardManager = (KeyguardManager) getCurrentActivity().getSystemService(KEYGUARD_SERVICE);
        PackageManager packageManager = getCurrentActivity().getPackageManager();

        if (!keyguardManager.isKeyguardSecure()) {
            new OKAlertDialog(getCurrentContext()) {

                @Override
                public boolean setCancelable() {
                    return true;
                }

                @Override
                public String setTitle() {
                    return getCurrentActivity().getResources().getString(R.string.not_configured_fingure);
                }

                @Override
                public String setMessage() {
                    return "Do you want to configure your finger to device ?";
                }

                @Override
                public Drawable setIcon() {
                    return getCurrentActivity().getDrawable(R.drawable.ic_fingerprint);
                }

                @Override
                public String setPositiveButtonText() {
                    return "Yes";
                }

                @Override
                public DialogInterface.OnClickListener setPositiveButtonClick() {
                    return (dialog, which) -> {
                        dialog.dismiss();
                        getCurrentActivity().startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                    };
                }

                @Override
                public String setNegativeButtonText() {
                    return "NO";
                }

                @Override
                public DialogInterface.OnClickListener setNegativeButtonClick() {
                    return (dialog, which) -> dialog.dismiss();
                }

                @Override
                public String setNeutralButtonText() {
                    return null;
                }

                @Override
                public DialogInterface.OnClickListener setNeutralButtonClick() {
                    return null;
                }

                @Override
                public boolean setAutoShow() {
                    return true;
                }

                @Override
                public DialogInterface.OnDismissListener setOnDismissListener() {
                    return null;
                }
            };
            showSnakbar("Lock screen security not enabled in Settings", false, getParentView());
            return false;
        }

        if (ActivityCompat.checkSelfPermission(getCurrentActivity(),
                Manifest.permission.USE_BIOMETRIC) !=
                PackageManager.PERMISSION_GRANTED) {
            showSnakbar("Fingerprint authentication permission not enabled", true, getParentView());
            return false;
        }

        if (packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
            return true;
        }
        return true;
    }

}
