package com.helpinghands.helperstations.hmutils.utils;

import org.apache.commons.codec.binary.Base64;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESEncrypt {

    private static final String ALGORITHM = "AES";
    static String keyValue12 = "tapp@@-!nvent@ry";

    static byte[] keyValue = keyValue12.getBytes();

    public static String encrypt(String valueToEnc) throws Exception {

        String encryptedValue = "";

        try {
            Key key = generateKey();
            Cipher c = Cipher.getInstance(ALGORITHM, "BC");
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encValue = c.doFinal(valueToEnc.getBytes("UTF8"));

            byte[] byteEncryptedValue = Base64.encodeBase64(encValue);

            encryptedValue = new String(byteEncryptedValue);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedValue;
    }

    public static String decrypt(String encryptedValue) throws Exception {
        String decryptedValue = "";
        try {
            Key key = generateKey();
            Cipher c = Cipher.getInstance(ALGORITHM, "BC");
            c.init(Cipher.DECRYPT_MODE, key);


            byte[] decordedValue = Base64.decodeBase64(encryptedValue.getBytes());
            byte[] decValue = c.doFinal(decordedValue);
            decryptedValue = new String(decValue);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedValue;
    }

    private static Key generateKey() {
        Key key = new SecretKeySpec(keyValue, ALGORITHM);
        return key;
    }

    public static void main(String[] arg) {
        try {
            String password = AESEncrypt.encrypt("hi");
            System.out.println(password);
            String painpassword = AESEncrypt.decrypt(password);
            System.out.println(painpassword);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
