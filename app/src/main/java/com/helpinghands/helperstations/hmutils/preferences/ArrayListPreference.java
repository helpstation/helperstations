package com.helpinghands.helperstations.hmutils.preferences;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Generic Preference Store Class for ArrayList<ListModels> </>which handles filtered values
 *
 * @param <ItemType> Data type of Your List
 */
public class ArrayListPreference<ItemType> extends ArrayList<ItemType> {

    private static ArrayListPreference arrayListPreference;

    public static ArrayListPreference getInstance() {
        if (arrayListPreference == null) {
            arrayListPreference = new ArrayListPreference();
        }
        return arrayListPreference;
    }

    public void addPreferenceArrayList(Activity activity, ArrayList<ItemType> arrayList, String key) {
        try {

            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            Gson gson = new Gson();

            String json = gson.toJson(arrayList);

            editor.putString(key, json);
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ArrayList<ItemType> getPreferenceArrayList(Activity activity, String key) {

        ArrayList<ItemType> arrayList = new ArrayList<>();

        try {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
            Gson gson = new Gson();
            String json = sharedPrefs.getString(key, null);
            Type type = new TypeToken<ArrayList<ItemType>>() {}.getType();
            arrayList = gson.fromJson(json, type);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return arrayList;
    }

    public void clearPreferenceArrayList(Activity activity, String key) {

        try {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(key, null);
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearArrayListPreferenceAll(Activity activity) {

        try {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.clear();
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
