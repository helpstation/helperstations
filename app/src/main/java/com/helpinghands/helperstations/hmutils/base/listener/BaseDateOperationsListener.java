package com.helpinghands.helperstations.hmutils.base.listener;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.google.android.material.textfield.TextInputLayout;
import com.helpinghands.helperstations.R;
import com.helpinghands.helperstations.hmutils.preferences.Preferences;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

public interface BaseDateOperationsListener extends BaseViewInterface {

    String yyyy_MM_dd_T_HH_mm_ss = "yyyy-MM-dd'T'HH:mm:ss";
    String dd_MM_yyyy_HH_mm_ss = "dd-MM-yyyy HH:mm:ss";
    String dd_MMM_yyyy = "dd-MMM-yyyy";
    String yyyy_MM_dd_T_HH_mm_ss_aa = "yyyy-MM-dd'T'HH:mm:ss aa";
    String yyyy_MM_dd_HH_mm_ss_aa_0 = "yyyy-MM-dd HH:mm:ss.0";
    String dd_MMM_yyyy_HH_aa = "dd MMM yyyy HH aa";
    String yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    String HH_mm = "HH:mm";
    String yyyy_MM_dd = "yyyy-MM-dd";
    String HH_mm_aa = "HH:mm aa";
    String HH_aa = "HH aa";
    String M_dd = "M/dd";
    String dd_MMM = "dd MMM";
    String dd_MMM_HH_mm_aa = "dd MMM, HH.mm aa";
    String MMM_dd_EEE = "MMM dd, EEE";
    String dd_MMM_yyyy_NO_DASH = "dd MMM yyyy";
    String dd_MMM_yyyy_HH_mm = "dd-MMM-yyyy HH:mm";

    default TimeZone getTimeZone() {
        String tZone = Preferences.getPreference().getString(getCurrentContext(), R.string.pref_timezone);
        if (TextUtils.isEmpty(tZone)) {
            return TimeZone.getTimeZone("GMT+05:30");
        }
        return TimeZone.getTimeZone(tZone);
    }

    default Date stringToDate(String sDate, String format, boolean needTimezoneConvert) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        if (needTimezoneConvert) {
            Calendar calendar = Calendar.getInstance();
            simpleDateFormat.setTimeZone(getTimeZone());
            simpleDateFormat.format(calendar.getTime());
        }

        Date date = null;
        try {
            date = simpleDateFormat.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    default String getCurrentDate(boolean needTimezoneConvert) {
        String entryDate = "";
        try {
            if (needTimezoneConvert) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(yyyy_MM_dd_T_HH_mm_ss, Locale.US);
                simpleDateFormat.setTimeZone(getTimeZone());
                simpleDateFormat.format(calendar.getTime());
                entryDate = simpleDateFormat.format(new Date());
            } else {
                entryDate = new SimpleDateFormat(yyyy_MM_dd_T_HH_mm_ss, Locale.US).format(new Date());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return entryDate;
    }

    default String dateFormat(String sDate, String givenPattern, String toPattern, boolean needTimezoneConvert) {
        Date dDate;
        DateFormat originalFormat = new SimpleDateFormat(givenPattern, Locale.US);
        DateFormat targetFormat = new SimpleDateFormat(toPattern, Locale.US);

        if (needTimezoneConvert) {
            Calendar calendar = Calendar.getInstance();
            originalFormat.setTimeZone(getTimeZone());
            originalFormat.format(calendar.getTime());
            targetFormat.setTimeZone(getTimeZone());
            targetFormat.format(calendar.getTime());
        }

        try {
            if (TextUtils.isEmpty(sDate)) {
                sDate = "";
            } else {
                dDate = originalFormat.parse(sDate);
                sDate = targetFormat.format(dDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sDate;
    }

    // returns true if fromdate is before todate
    default boolean checkDates(String fromDate, String toDate, String dateFormate, boolean needTimezoneConvert) {

        try {
            Date strFromDate = null, strToDate = null;

            SimpleDateFormat sdf = new SimpleDateFormat(dateFormate, Locale.US);

            if (needTimezoneConvert) {
                Calendar calendar = Calendar.getInstance();
                sdf.setTimeZone(getTimeZone());
                sdf.format(calendar.getTime());
            }

            if (!TextUtils.isEmpty(fromDate)) {

                strFromDate = sdf.parse(fromDate);
            }

            if (!TextUtils.isEmpty(toDate)) {

                strToDate = sdf.parse(toDate);
            }

            if (!TextUtils.isEmpty(fromDate) && !TextUtils.isEmpty(toDate)) {

                if (strFromDate != null && (strFromDate.before(strToDate) || strFromDate.equals(strToDate))) {
                    return true;
                }
            } else {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    default boolean checkDatesEqual(String fromDate, String toDate, String dateFormate, boolean needTimezoneConvert) {

        try {
            Date strFromDate = null, strToDate = null;

            SimpleDateFormat sdf = new SimpleDateFormat(dateFormate, Locale.US);

            if (needTimezoneConvert) {
                Calendar calendar = Calendar.getInstance();
                sdf.setTimeZone(getTimeZone());
                sdf.format(calendar.getTime());
            }

            if (!TextUtils.isEmpty(fromDate)) {

                strFromDate = sdf.parse(fromDate);
            }

            if (!TextUtils.isEmpty(toDate)) {

                strToDate = sdf.parse(toDate);
            }

            if (!TextUtils.isEmpty(fromDate) && !TextUtils.isEmpty(toDate)) {

                if (strFromDate != null && strFromDate.equals(strToDate)) {
                    return true;
                }
            } else {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /*** Method to add days in current date ***/
    default String addDaysInDate(int days, boolean needTimezoneConvert) {
        String newDate = "";
        try {
            SimpleDateFormat postFormatter = new SimpleDateFormat(yyyy_MM_dd, Locale.US);
            Calendar calendar = Calendar.getInstance();
            if (needTimezoneConvert) {
                postFormatter.setTimeZone(getTimeZone());
                postFormatter.format(calendar.getTime());
            }
            calendar.add(Calendar.DAY_OF_MONTH, +days);
            Date date = new Date();
            date.setTime(calendar.getTimeInMillis());
            newDate = postFormatter.format(date);
            return newDate;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return newDate;
    }

    /*** Method to subtract days in current date ***/
    default String subtractDaysInDate(int days, boolean needTimezoneConvert) {

        String newDate = "";

        try {

            Calendar calender = Calendar.getInstance();
            SimpleDateFormat postFormatter = new SimpleDateFormat(yyyy_MM_dd, Locale.US);
            if (needTimezoneConvert) {
                postFormatter.setTimeZone(getTimeZone());
                postFormatter.format(calender.getTime());
            }
            calender.add(Calendar.DAY_OF_MONTH, -days);
            Date date = new Date();
            date.setTime(calender.getTimeInMillis());
            newDate = postFormatter.format(date);
            return newDate;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return newDate;
    }

    /*** Method to subtract days in current date ***/
    default String subtractMonth(int month, boolean needTimezoneConvert) {

        String newDate = "";

        try {

            Calendar calender = Calendar.getInstance();
            SimpleDateFormat postFormatter = new SimpleDateFormat(yyyy_MM_dd, Locale.US);
            if (needTimezoneConvert) {
                postFormatter.setTimeZone(getTimeZone());
                postFormatter.format(calender.getTime());
            }
            calender.add(Calendar.MONTH, -month);
            Date date = new Date();
            date.setTime(calender.getTimeInMillis());
            newDate = postFormatter.format(date);
            return newDate;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return newDate;
    }

    /*** Method to subtract days in current date ***/
    default String subtractYear(int year, boolean needTimezoneConvert) {

        String newDate = "";

        try {

            Calendar calender = Calendar.getInstance();
            SimpleDateFormat postFormatter = new SimpleDateFormat(yyyy_MM_dd, Locale.US);
            if (needTimezoneConvert) {
                postFormatter.setTimeZone(getTimeZone());
                postFormatter.format(calender.getTime());
            }
            calender.add(Calendar.YEAR, -year);
            Date date = new Date();
            date.setTime(calender.getTimeInMillis());
            newDate = postFormatter.format(date);
            return newDate;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return newDate;
    }

    default Date addDaysExcludingWeekEnds(Date inputDate, int incrementDays) {

        Calendar c = Calendar.getInstance();
        c.setTime(inputDate);

        int workDays = 0;

        do {

            c.add(Calendar.DAY_OF_MONTH, 1);

            if (c.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && c.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                ++workDays;
            }
        }
        while (workDays < incrementDays);

        return c.getTime();
    }

    default int getTotalMinutes(String time) {

        String[] t = new String[0];
        try {
            t = time.split(":");
            if (t != null && t.length > 0)
                return Integer.valueOf(t[0]) * 60 + Integer.valueOf(t[1]);
            else
                return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    default int getTotalSeconds(String time) {

        try {
            String[] t = time.split(":");
            if (t != null && t.length > 0)
                return (Integer.valueOf(t[0]) * 60 * 60) + (Integer.valueOf(t[1]) * 60) + Integer.valueOf(t[2]);
            else
                return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    default String getTimeFromMinutes(int total, String format) {
        int minutes = total % 60;
        int hours;
        if (total == 1440) {
            hours = 24;
        } else if (total - minutes > 1440) {
            hours = ((total - minutes) / 60);
        } else {
            hours = ((total - minutes) / 60) % 24;
        }
        return String.format(Locale.US, format, hours, minutes);
    }

    default String getTimeFromSec(int total, String format) {
        int hours = total / 3600;
        int minutes = (total % 3600) / 60;
        int seconds = total % 60;

        return String.format(Locale.US, format, hours, minutes, seconds);
    }

    default void showTimePickerDialog(View view, String time) {
        try {
            if (view != null) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();

                int hour, minute;
                if (time != null) {
                    String time1[] = time.split(":");
                    hour = Integer.parseInt(time1[0]);
                    minute = Integer.parseInt(time1[1]) + 1;
                } else {
                    hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    minute = mcurrentTime.get(Calendar.MINUTE);
                }
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getCurrentActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if (view instanceof TextInputLayout) {
                            TextInputLayout textInputLayout = (TextInputLayout) view;
                            Objects.requireNonNull(textInputLayout.getEditText()).setText(String.valueOf(selectedHour).concat(":").concat(String.valueOf(selectedMinute)));
                        } else if (view instanceof EditText) {
                            EditText editText = (EditText) view;
                            Objects.requireNonNull(editText).setText(String.valueOf(selectedHour).concat(":").concat(String.valueOf(selectedMinute)));
                        }
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    default void showDatePickerDialog(final View view, final String myFormat) {
        try {
            if (view != null) {
                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker myView, int year, int monthOfYear, int dayOfMonth) {
                        String selectedDate = "";
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        selectedDate = sdf.format(myCalendar.getTime());

                        if (view instanceof TextInputLayout) {
                            TextInputLayout textInputLayout = (TextInputLayout) view;
                            Objects.requireNonNull(textInputLayout.getEditText()).setText(selectedDate);
                        } else if (view instanceof EditText) {
                            EditText editText = (EditText) view;
                            Objects.requireNonNull(editText).setText(selectedDate);
                        }
                    }
                };
                new DatePickerDialog(getCurrentActivity(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*** Method to add sec in current time ***/
    default String addSecInTime(int sec, String currentTime, boolean needTimezoneConvert) {
        String newTime = "";
        try {
            SimpleDateFormat postFormatter = new SimpleDateFormat(yyyy_MM_dd_T_HH_mm_ss, Locale.US);
            Calendar calendar = Calendar.getInstance();
            Date date = null;
            try {
                date = postFormatter.parse(currentTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.setTime(date);
            if (needTimezoneConvert) {
                postFormatter.setTimeZone(getTimeZone());
                postFormatter.format(calendar.getTime());
            }

            calendar.add(Calendar.SECOND, sec);
            Date date1 = new Date();
            date1.setTime(calendar.getTimeInMillis());
            newTime = postFormatter.format(date1);
            return newTime;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return newTime;
    }

    //1 minute = 60 seconds
    //1 hour = 60 x 60 = 3600
    //1 day = 3600 x 24 = 86400
    class GetTimeDiffeerence {

        public String sDays = "", sHours = "", sMinutes = "", sSeconds = "";

        public GetTimeDiffeerence(String startDat, String endDat, String formate, Context context, boolean needTimezoneConvert) {

            Log.e("TAG", "Time:: STARTDATE::" + startDat);
            Log.e("TAG", "Time:: END Date::" + endDat);
            Date startDate = null;
            Date endDate = null;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formate, Locale.US);

            if (needTimezoneConvert) {
                String tZone = Preferences.getPreference().getString(context, R.string.pref_timezone);
                Calendar calendar = Calendar.getInstance();
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone(tZone));
                simpleDateFormat.format(calendar.getTime());
            }

            try {
                startDate = simpleDateFormat.parse(startDat);
                endDate = simpleDateFormat.parse(endDat);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //milliseconds
            long different = 0;
            if (startDate != null && endDate != null) {
                different = endDate.getTime() - startDate.getTime();
            }

            System.out.println("startDate : " + startDate);
            System.out.println("endDate : " + endDate);
            System.out.println("different : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            elapsedSeconds = (((different / secondsInMilli) == -1) ? 0 : (different / secondsInMilli));

            sDays = String.format(Locale.US, "%d", elapsedDays);
            sHours = String.format(Locale.US, "%d", elapsedHours);
            sMinutes = String.format(Locale.US, "%d", elapsedMinutes);
            sSeconds = String.format(Locale.US, "%d", elapsedSeconds);

            Log.d("TAG", "Time:: day::" + elapsedDays);
            Log.d("TAG", "Time:: hour::" + elapsedHours);
            Log.d("TAG", "Time:: min::" + elapsedMinutes);
            Log.d("TAG", "Time:: sec::" + elapsedSeconds);
        }
    }

    class AgeCalculator {

        public Age calculateAge(Date birthDate) {
            int years = 0;
            int months = 0;
            int days = 0;

            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());

            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);

            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;

            //Get difference between months
            months = currMonth - birthMonth;

            //if month difference is in negative then reduce years by one
            //and calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }

            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else {
                days = 0;
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }
            //Create new Age object
            return new Age(days, months, years);
        }

        public class Age {
            private int days;
            private int months;
            private int years;

            private Age() {
                //Prevent default constructor
            }

            public Age(int days, int months, int years) {
                this.days = days;
                this.months = months;
                this.years = years;
            }

            public int getDays() {
                return this.days;
            }

            public int getMonths() {
                return this.months;
            }

            public int getYears() {
                return this.years;
            }

            @Override
            public String toString() {
                return years + " Years, " + months + " Months, " + days + " Days";
            }
        }
    }
}
