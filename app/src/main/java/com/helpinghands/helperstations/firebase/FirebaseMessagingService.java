package com.helpinghands.helperstations.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.helpinghands.helperstations.R;


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private static final String TAG = "HSFirebaseService";

    public static void sendNotification(Context context, String messageTitle, String messageBody, Intent intent, RemoteMessage remoteMessage) {

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        int requestCode = (int) System.currentTimeMillis();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationManager notifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notifManager != null;
            NotificationChannel mChannel = notifManager
                    .getNotificationChannel(context.getResources().getString(R.string.notification_channel_id));

            if (mChannel == null) {
                mChannel = new NotificationChannel(context.getResources().getString(R.string.notification_channel_id),
                        context.getResources().getString(R.string.notification_channel_name),
                        NotificationManager.IMPORTANCE_HIGH);
                mChannel.setDescription(context.getResources().getString(R.string.notification_channel_description));
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{200, 200, 200, 200, 200});
                notifManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext(), context.getResources().getString(R.string.notification_channel_id));
            PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), requestCode, intent, PendingIntent.FLAG_ONE_SHOT);

            builder.setContentTitle(messageTitle)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentText(messageBody)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setColor(context.getResources().getColor(R.color.colorAccent))
                    .setContentIntent(pendingIntent)
                    .setTicker(messageBody)
                    .setVibrate(new long[]{200, 200, 200, 200, 200});

            Notification notification = builder.build();
            notifManager.notify(requestCode, notification);

        } else {

            PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), requestCode, intent, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            Notification notification = new Notification.Builder(context.getApplicationContext())
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVibrate(new long[]{200, 200, 200, 200, 200})
                    .build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.notify(requestCode, notification);
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        Intent intent = null;

        try {
            Log.d(TAG, "remoteMessage: " + remoteMessage);

//            JSONObject jsonObject = new JSONObject(remoteMessage.getData());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
